#include "CurvedHelixTrajectoryGenerator.h"
#include "CurvedDetectorDescriptor.h"

#include <optional>

namespace elsa
{
    std::unique_ptr<CurvedDetectorDescriptor> CurvedHelixTrajectoryGenerator::createTrajectory(
        const DataDescriptor& volumeDescriptor, std::vector<real_t> thetas, real_t pitch,
        real_t sourceToCenter, real_t centerToDetector, geometry::Radian angle,
        std::optional<RealVector_t> principalPointOffset,
        std::optional<RealVector_t> centerOfRotOffset, std::optional<IndexVector_t> detectorSize,
        std::optional<RealVector_t> detectorSpacing)
    {
        auto [coeffs, spacing, geometryList] = BaseHelixTrajectoryGenerator::createTrajectoryData(
            volumeDescriptor, thetas, pitch, sourceToCenter, centerToDetector, principalPointOffset,
            centerOfRotOffset, detectorSize, detectorSpacing);

        return std::make_unique<CurvedDetectorDescriptor>(std::move(coeffs), std::move(spacing),
                                                          std::move(geometryList), angle,
                                                          sourceToCenter + centerToDetector);
    }
} // namespace elsa
