#include "FBP.h"
#include "DataContainer.h"
#include "Filter.h"
#include "FourierTransform.h"
#include "TypeCasts.hpp"
#include <Eigen/src/Core/util/Constants.h>

namespace elsa
{
    template <typename data_t>
    FBP<data_t>::FBP(const LinearOperator<data_t>& P, const Filter<data_t>& g)
        : projector_{P.clone()}, filter_{downcast<Filter<data_t>>(g.clone())}
    {
    }

    template <typename data_t>
    FBP<data_t>::FBP(const LinearOperator<data_t>& P, std::unique_ptr<Filter<data_t>> g)
        : projector_{P.clone()}, filter_{std::move(g)}
    {
    }

    template <typename data_t>
    DataContainer<data_t> FBP<data_t>::apply(const DataContainer<data_t>& sinogram) const
    {
        auto& descriptor = sinogram.getDataDescriptor();
        auto dim = descriptor.getNumberOfDimensions();

        if (dim != 2) {
            throw InvalidArgumentError("FBP.apply:: only handles 2D data");
        }

        auto filtered = empty<data_t>(descriptor);
        auto numSlices = descriptor.getNumberOfCoefficientsPerDimension()[dim - 1];

        PartitionDescriptor sliceDescriptor{descriptor, numSlices};
        FourierTransform<complex<data_t>> F{sliceDescriptor.getDescriptorOfBlock(0)};

        for (auto i = 0; i < numSlices; i++) {
            auto spectrum = F.apply(sinogram.slice(i).asComplex());
            auto filteredRow = F.applyAdjoint(filter_->apply(spectrum));
            filtered.slice(i) = elsa::real(filteredRow);
        }

        auto backprojected = projector_->applyAdjoint(filtered);
        return backprojected * pi_t / 2 / numSlices; // This normalization is necessary because the
                                                     // projectors are not normalized
                                                     // TODO: Compare normalization with literature
    }

    // ------------------------------------------
    // explicit template instantiation
    template class FBP<float>;
    template class FBP<double>;

} // namespace elsa
