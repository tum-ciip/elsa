/**
 * @file test_AXDTOperator.cpp
 *
 * @brief Tests for the ADXTOperator
 *
 * @author Shen Hu - main code
 */

#include "SphericalCoefficientsDescriptor.h"
#include "doctest/doctest.h"
#include "SphericalHarmonicsTransform.h"
#include "IdenticalBlocksDescriptor.h"
#include "DataContainer.h"
#include "Logger.h"
#include "testHelpers.h"

using namespace elsa;
using namespace doctest;
using namespace elsa::axdt;

TEST_SUITE_BEGIN("core");

TEST_CASE_TEMPLATE("Testing SphericalFieldsTransform", data_t, float, double)
{
    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);
    srand((unsigned int) 666);

    const size_t volSize{2};
    IndexVector_t vol(2);
    vol << volSize, volSize;
    VolumeDescriptor volDesc{vol};

    axdt::DirVecList<data_t> samplingPattern;
    samplingPattern.emplace_back(0.50748, -0.3062, 0.80543);
    samplingPattern.emplace_back(-0.3062, 0.80543, 0.50748);
    samplingPattern.emplace_back(-0.50748, 0.3062, 0.80543);
    samplingPattern.emplace_back(0.80543, 0.50748, -0.3062);
    samplingPattern.emplace_back(0.3062, 0.80543, -0.50748);
    samplingPattern.emplace_back(0.80543, -0.50748, 0.3062);
    samplingPattern.emplace_back(0.3062, -0.80543, 0.50748);
    samplingPattern.emplace_back(-0.80543, -0.50748, -0.3062);
    samplingPattern.emplace_back(-0.3062, -0.80543, -0.50748);
    samplingPattern.emplace_back(-0.80543, 0.50748, 0.3062);
    samplingPattern.emplace_back(0.50748, 0.3062, -0.80543);
    samplingPattern.emplace_back(-0.50748, -0.3062, -0.80543);
    samplingPattern.emplace_back(0.62636, -0.24353, -0.74052);
    samplingPattern.emplace_back(-0.24353, -0.74052, 0.62636);
    samplingPattern.emplace_back(-0.62636, 0.24353, -0.74052);
    samplingPattern.emplace_back(-0.74052, 0.62636, -0.24353);
    samplingPattern.emplace_back(0.24353, -0.74052, -0.62636);
    samplingPattern.emplace_back(-0.74052, -0.62636, 0.24353);
    samplingPattern.emplace_back(0.24353, 0.74052, 0.62636);
    samplingPattern.emplace_back(0.74052, -0.62636, -0.24353);
    samplingPattern.emplace_back(-0.24353, 0.74052, -0.62636);
    samplingPattern.emplace_back(0.74052, 0.62636, 0.24353);
    samplingPattern.emplace_back(0.62636, 0.24353, 0.74052);
    samplingPattern.emplace_back(-0.62636, -0.24353, 0.74052);
    samplingPattern.emplace_back(-0.28625, 0.95712, -0.044524);
    samplingPattern.emplace_back(0.95712, -0.044524, -0.28625);
    samplingPattern.emplace_back(0.28625, -0.95712, -0.044524);
    samplingPattern.emplace_back(-0.044524, -0.28625, 0.95712);
    samplingPattern.emplace_back(-0.95712, -0.044524, 0.28625);
    samplingPattern.emplace_back(-0.044524, 0.28625, -0.95712);
    samplingPattern.emplace_back(-0.95712, 0.044524, -0.28625);
    samplingPattern.emplace_back(0.044524, 0.28625, 0.95712);
    samplingPattern.emplace_back(0.95712, 0.044524, 0.28625);
    samplingPattern.emplace_back(0.044524, -0.28625, -0.95712);
    samplingPattern.emplace_back(-0.28625, -0.95712, 0.044524);
    samplingPattern.emplace_back(0.28625, 0.95712, 0.044524);

    Vector_t<data_t> weights = static_cast<real_t>(4.0 * pi<data_t>)
                               / static_cast<real_t>(samplingPattern.size())
                               * Vector_t<data_t>::Ones(asSigned(samplingPattern.size()));

    const auto symmetry = axdt::Symmetry::regular;
    const auto maxL = 4;

    auto samplingDirCnt{samplingPattern.size()};
    auto sphCoeffsCnt{SphericalCoefficientsDescriptor::coefficientCount(symmetry, maxL)};

    GIVEN("sf measurements and ground truth transform results")
    {
        Vector_t<double> input(samplingDirCnt);
        input << 0.754358291407775, -0.306820645184338, -0.123204435921810, 0.849556048213156,
            -1.274938108853798, -0.573111100611586, 0.627576237773633, 0.177121393717249,
            0.104396351299045, 0.142036209457050, 2.231368511612128, -0.015059877844922,
            0.805849500639438, -1.085409856384089, 0.325126697713267, 0.238064050790899,
            -1.072604254662862, -0.519390459070897, 0.020086630029301, -1.463706932543725,
            -0.424752454749628, 0.120423823957031, 0.930582112066446, 0.338658848788792,
            0.147279798616856, 0.756725110766516, 0.881930324545221, -0.087410216765013,
            1.031659345636263, 0.864097492602849, 0.401666595949420, -0.112340125336992,
            0.129707228273439, -0.081263651974596, 0.251449774448726, -0.909181928284521;

        // matlab forward transform result
        Vector_t<double> matlab_result(sphCoeffsCnt);
        matlab_result << 0.401808033751943, 0.075966691690841, 0.239916153553659, 0.123318934835165,
            0.183907788282417, 0.239952525664901, 0.417267069084371, 0.049654430325743,
            0.902716109915281, 0.944787189721647, 0.490864092468080, 0.489252638400017,
            0.337719409821378, 0.900053846417662, 0.369246781120215, 0.111202755293787,
            0.780252068321136, 0.389738836961254, 0.241691285913831, 0.403912145588112,
            0.096454525168390, 0.131973292606336, 0.942050590775488, 0.956134540229803,
            0.575208595078468;

        auto spfFieldDesc = std::make_unique<IdenticalBlocksDescriptor>(samplingDirCnt, volDesc);
        auto spfWeights = std::make_unique<DataContainer<data_t>>(*spfFieldDesc);
        const auto& weightVolDesc = spfFieldDesc->getDescriptorOfBlock(0);

        auto sphWeightsDesc =
            std::make_unique<IdenticalBlocksDescriptor>(sphCoeffsCnt, weightVolDesc);
        auto sphWeights = std::make_unique<DataContainer<data_t>>(*sphWeightsDesc);

        index_t volCnt = weightVolDesc.getNumberOfCoefficients();

        WHEN("apply arbitrary weights and perform the transform")
        {
            for (index_t z = 0; z < asSigned(samplingDirCnt); ++z) {
                for (index_t y = 0; y < asSigned(volSize); ++y) {
                    for (index_t x = 0; x < asSigned(volSize); ++x) {
                        IndexVector_t idx(3);
                        idx << x, y, z;
                        (*spfWeights)(idx) =
                            static_cast<data_t>(2 * y + x + 1) * static_cast<data_t>(input[z]);
                    }
                }
            }

            Eigen::Map<const Matrix_t<data_t>> x(&((spfWeights->storage())[0]), volCnt,
                                                 asSigned(samplingDirCnt));

            Eigen::Map<Matrix_t<data_t>> Ax(&((sphWeights->storage())[0]), volCnt, sphCoeffsCnt);

            Matrix_t<data_t> transformToSPH =
                sqrt(weights.size() / weights.sum()) * weights.asDiagonal()
                * axdt::evalSphericalHarmonics(symmetry, maxL, samplingPattern);

            Ax = x * transformToSPH;
            THEN("results should be close to ground truth")
            {
                for (index_t z = 0; z < asSigned(sphCoeffsCnt); ++z) {
                    for (index_t y = 0; y < asSigned(volSize); ++y) {
                        for (index_t x = 0; x < asSigned(volSize); ++x) {
                            IndexVector_t idx(3);
                            idx << x, y, z;
                            REQUIRE((*sphWeights)(idx)
                                    == Approx(static_cast<data_t>(2 * y + x + 1) * matlab_result[z])
                                           .epsilon(1e-3));
                        }
                    }
                }
            }
        }
    }
}

TEST_CASE_TEMPLATE("Testing orthogonality of SphericalFieldsTransform", data_t, float, double)
{
    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);
    srand((unsigned int) 666);

    const size_t volSize{20};
    IndexVector_t vol{{volSize, volSize, volSize}};
    VolumeDescriptor volDesc{vol};

    axdt::DirVecList<data_t> samplingPattern;
    samplingPattern.emplace_back(0.50748, -0.3062, 0.80543);
    samplingPattern.emplace_back(-0.3062, 0.80543, 0.50748);
    samplingPattern.emplace_back(-0.50748, 0.3062, 0.80543);
    samplingPattern.emplace_back(0.80543, 0.50748, -0.3062);
    samplingPattern.emplace_back(0.3062, 0.80543, -0.50748);
    samplingPattern.emplace_back(0.80543, -0.50748, 0.3062);
    samplingPattern.emplace_back(0.3062, -0.80543, 0.50748);
    samplingPattern.emplace_back(-0.80543, -0.50748, -0.3062);
    samplingPattern.emplace_back(-0.3062, -0.80543, -0.50748);
    samplingPattern.emplace_back(-0.80543, 0.50748, 0.3062);
    samplingPattern.emplace_back(0.50748, 0.3062, -0.80543);
    samplingPattern.emplace_back(-0.50748, -0.3062, -0.80543);
    samplingPattern.emplace_back(0.62636, -0.24353, -0.74052);
    samplingPattern.emplace_back(-0.24353, -0.74052, 0.62636);
    samplingPattern.emplace_back(-0.62636, 0.24353, -0.74052);
    samplingPattern.emplace_back(-0.74052, 0.62636, -0.24353);
    samplingPattern.emplace_back(0.24353, -0.74052, -0.62636);
    samplingPattern.emplace_back(-0.74052, -0.62636, 0.24353);
    samplingPattern.emplace_back(0.24353, 0.74052, 0.62636);
    samplingPattern.emplace_back(0.74052, -0.62636, -0.24353);
    samplingPattern.emplace_back(-0.24353, 0.74052, -0.62636);
    samplingPattern.emplace_back(0.74052, 0.62636, 0.24353);
    samplingPattern.emplace_back(0.62636, 0.24353, 0.74052);
    samplingPattern.emplace_back(-0.62636, -0.24353, 0.74052);
    samplingPattern.emplace_back(-0.28625, 0.95712, -0.044524);
    samplingPattern.emplace_back(0.95712, -0.044524, -0.28625);
    samplingPattern.emplace_back(0.28625, -0.95712, -0.044524);
    samplingPattern.emplace_back(-0.044524, -0.28625, 0.95712);
    samplingPattern.emplace_back(-0.95712, -0.044524, 0.28625);
    samplingPattern.emplace_back(-0.044524, 0.28625, -0.95712);
    samplingPattern.emplace_back(-0.95712, 0.044524, -0.28625);
    samplingPattern.emplace_back(0.044524, 0.28625, 0.95712);
    samplingPattern.emplace_back(0.95712, 0.044524, 0.28625);
    samplingPattern.emplace_back(0.044524, -0.28625, -0.95712);
    samplingPattern.emplace_back(-0.28625, -0.95712, 0.044524);
    samplingPattern.emplace_back(0.28625, 0.95712, 0.044524);

    const auto symmetry = axdt::Symmetry::even;
    const auto maxL = 4;

    SphericalCoefficientsDescriptor coeffDesc{volDesc, symmetry, maxL};

    GIVEN("a random vector")
    {
        SphericalHarmonicsTransform<data_t> A{coeffDesc, samplingPattern};

        auto [x, unused] = generateRandomContainer<data_t>(coeffDesc);

        WHEN("applying the transform followed by its adjoint")
        {
            auto Ax = A.apply(x);
            auto ATAx = A.applyAdjoint(Ax);

            THEN("the result should be close to the original vector")
            {
                REQUIRE_UNARY(isCwiseApprox(x, ATAx));
            }
        }
    }
}

TEST_SUITE_END();
