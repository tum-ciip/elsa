#include "DataContainer.h"
#include "IdenticalBlocksDescriptor.h"
#include "Math.hpp"
#include "SphericalCoefficientsDescriptor.h"
#include "TypeCasts.hpp"
#include "elsaDefines.h"
#include "SphericalHarmonicsTransform.h"
#include "XGIDetectorDescriptor.h"

namespace elsa::axdt
{

    namespace detail
    {

        template <typename data_t>
        inline std::array<data_t, 15> exactCoeffsMalecki(const DirVec<data_t> ray,
                                                         const DirVec<data_t> sensitivity)
        {
            using math::sq;

            const data_t lx = ray[0], ly = ray[1], lz = ray[2];
            const data_t tx = sensitivity[0], ty = sensitivity[1], tz = sensitivity[2];

            // Thanks, Mathematica!
            data_t h_00 =
                (4 * sqrt(pi<data_t>)
                 * (1 + sq(ty) - 2 * lx * lz * tx * tz + sq(tz) - 2 * ly * ty * (lx * tx + lz * tz)
                    - sq(ly) * (-1 + 2 * sq(ty) + sq(tz)) - sq(lz) * (-1 + sq(ty) + 2 * sq(tz))))
                / 15.;

            data_t h_2_2 = (4 * sqrt(pi<data_t> / 15.)
                            * (2 * tx * ((2 + sq(lz)) * ty - ly * lz * tz)
                               + lx * (-2 * lz * ty * tz + ly * (-3 + 2 * sq(tz)))))
                           / 7.;

            data_t h_2_1 = (4 * sqrt(pi<data_t> / 15.)
                            * (2 * sq(ly) * ty * tz + 2 * ty * (lx * lz * tx + (-3 + sq(lz)) * tz)
                               + ly * (2 * lx * tx * tz + lz * (1 + 2 * sq(ty) + 2 * sq(tz)))))
                           / 7.;

            data_t h_20 =
                (2 * sqrt(pi<data_t> / 5.)
                 * (-1 - 4 * sq(ly) - 7 * sq(lz) + 8 * lx * ly * tx * ty
                    + 4 * (-1 + 2 * sq(ly) + sq(lz)) * sq(ty) - 4 * lz * (lx * tx + ly * ty) * tz
                    + 2 * (7 + 2 * sq(ly) - 2 * sq(lz)) * sq(tz)))
                / 21.;

            data_t h_21 = (-4 * sqrt(pi<data_t> / 15.)
                           * (2 * tx * (-(ly * lz * ty) + (2 + sq(ly)) * tz)
                              + lx * (lz * (-3 + 2 * sq(ty)) - 2 * ly * ty * tz)))
                          / 7.;

            data_t h_22 =
                (-2 * sqrt(pi<data_t> / 15.)
                 * (-1 + 8 * sq(ty) + 4 * lx * lz * tx * tz - 4 * ly * lz * ty * tz + 2 * sq(tz)
                    + sq(ly) * (-6 + 4 * sq(tz)) + sq(lz) * (-5 + 4 * sq(ty) + 4 * sq(tz))))
                / 7.;

            data_t h_4_4 =
                (4 * sqrt(pi<data_t> / 35.)
                 * ((-1 + 2 * sq(ly) + sq(lz)) * tx * ty + lx * ly * (-1 + 2 * sq(ty) + sq(tz))))
                / 3.;

            data_t h_4_3 = (-2 * sqrt((2 * pi<data_t>) / 35.)
                            * (2 * sq(ly) * ty * tz + ty * (-2 * lx * lz * tx + (-1 + sq(lz)) * tz)
                               + ly * (-2 * lx * tx * tz + lz * (-1 + 2 * sq(ty) + sq(tz)))))
                           / 3.;

            data_t h_4_2 = (-4 * sqrt(pi<data_t> / 5.)
                            * (tx * ((-1 + 3 * sq(lz)) * ty + 4 * ly * lz * tz)
                               + lx * (4 * lz * ty * tz + ly * (-1 + 3 * sq(tz)))))
                           / 21.;

            data_t h_4_1 =
                (-2 * sqrt((2 * pi<data_t>) / 5.)
                 * (2 * sq(ly) * ty * tz + ty * (2 * lx * lz * tx + (1 - 5 * sq(lz)) * tz)
                    + ly * (2 * lx * tx * tz + lz * (1 + 2 * sq(ty) - 5 * sq(tz)))))
                / 21.;

            data_t h_40 =
                (2 * sqrt(pi<data_t>)
                 * (-3 + 2 * sq(ly) + 7 * sq(lz) - 4 * lx * ly * tx * ty
                    - 2 * (-1 + 2 * sq(ly) + sq(lz)) * sq(ty) + 16 * lz * (lx * tx + ly * ty) * tz
                    + (7 - 2 * sq(ly) - 19 * sq(lz)) * sq(tz)))
                / 105.;

            data_t h_41 = (2 * sqrt((2 * pi<data_t>) / 5.)
                           * (tx * (-2 * ly * lz * ty + (-3 + 2 * sq(ly) + 7 * sq(lz)) * tz)
                              + lx * (-2 * ly * ty * tz + lz * (-3 + 2 * sq(ty) + 7 * sq(tz)))))
                          / 21.;

            data_t h_42 =
                (4 * sqrt(pi<data_t> / 5.)
                 * (1 - sq(ty) - 4 * lx * lz * tx * tz + 4 * ly * lz * ty * tz - 2 * sq(tz)
                    + sq(ly) * (-1 + 3 * sq(tz)) + sq(lz) * (-2 + 3 * sq(ty) + 3 * sq(tz))))
                / 21.;

            data_t h_43 = (-2 * sqrt((2 * pi<data_t>) / 35.)
                           * (tx * (2 * ly * lz * ty + (-1 + 2 * sq(ly) + sq(lz)) * tz)
                              + lx * (2 * ly * ty * tz + lz * (-1 + 2 * sq(ty) + sq(tz)))))
                          / 3.;

            data_t h_44 = (-2 * sqrt(pi<data_t> / 35.)
                           * (-4 * lx * ly * tx * ty + 2 * sq(ly) * (-1 + 2 * sq(ty) + sq(tz))
                              + (-1 + sq(lz)) * (-1 + 2 * sq(ty) + sq(tz))))
                          / 3.;

            return {h_00,  h_2_2, h_2_1, h_20, h_21, h_22, h_4_4, h_4_3,
                    h_4_2, h_4_1, h_40,  h_41, h_42, h_43, h_44};
        }
    } // namespace detail

    template <typename data_t>
    DataContainer<data_t> exactWeightingFunction(const XGIDetectorDescriptor& detectorDescriptor,
                                                 const Symmetry symmetry, const index_t degree)
    {

        const index_t poses = detectorDescriptor.getNumberOfCoefficientsPerDimension()[2];

        // setup complete block descriptor
        SphericalCoefficientsDescriptor resultDescriptor{detectorDescriptor, symmetry, degree};

        // setup factors block
        // Result has shape (detX×detY)×#poses×#coeffs
        // Set to zero as odd weights are zero and appear in Symmetry::regular
        auto weights = zeros<data_t>(resultDescriptor);

        if (detectorDescriptor.isParallelBeam()) {

#pragma omp parallel for
            for (index_t pose = 0; pose < poses; ++pose) {

                // obtain geometry object for current image
                const Geometry camera = detectorDescriptor.getGeometryAt(pose);

                // beam direction: last column of rotation matrix (scaling is forced to be
                // isotropic, z-direction/viewing axis)
                const DirVec<data_t> l =
                    camera.getRotationMatrix().transpose().col(2).cast<data_t>();

                // sensitivity direction: first column of rotation matrix (scaling is forced to
                // be isotropic, x-direction/horizontal axis)
                const DirVec<data_t> t =
                    (camera.getRotationMatrix().transpose() * detectorDescriptor.getSensDir())
                        .cast<data_t>();

                const auto coeffs = detail::exactCoeffsMalecki(l, t);

                if (symmetry == Symmetry::regular) {
                    // Truncation at l = 4 is exact
                    const auto lMax = std::min(degree, index_t{4});
                    auto j = 0;
                    for (int l = 0; l <= lMax; l += 2) {
                        for (int m = -l; m <= l; ++m) {
                            weights.getBlock(l * l + l + m).slice(pose).fill(coeffs[j]);
                            j++;
                        }
                    }

                } else if (symmetry == Symmetry::even) {
                    const auto coeffCount = std::min(
                        SphericalCoefficientsDescriptor::coefficientCount(symmetry, degree),
                        index_t{15});
                    for (auto j = 0; j < coeffCount; ++j) {
                        weights.getBlock(j).slice(pose).fill(coeffs[j]);
                    }
                }
            }
        } else {
            const index_t detX = detectorDescriptor.getNumberOfCoefficientsPerDimension()[0];
            const index_t detY = detectorDescriptor.getNumberOfCoefficientsPerDimension()[1];

#pragma omp parallel for
            for (index_t pose = 0; pose < poses; ++pose) {
                // set index to base for currently processed image and direction
                index_t idx = pose * detX * detY;

                // obtain geometry object for current image
                const auto camera = detectorDescriptor.getGeometryAt(pose);

                // sensitivity direction: first column of rotation matrix (scaling is
                // forced to be isotropic, x-direction/horizontal axis)
                const DirVec<data_t> t =
                    (camera.getRotationMatrix().transpose() * detectorDescriptor.getSensDir())
                        .cast<data_t>();

                // traverse image
                for (index_t y = 0; y < detY; ++y) {
                    for (index_t x = 0; x < detX; ++x) {
                        // sampling direction: set above, normalized by design
                        // beam direction: get ray as provided by projection matrix

                        const IndexVector_t coord{{x, y, pose}};

                        const auto ray = detectorDescriptor.computeRayFromDetectorCoord(coord);

                        const DirVec<data_t> l = ray.direction().cast<data_t>();

                        const auto coeffs = detail::exactCoeffsMalecki(l, t);

                        if (symmetry == Symmetry::regular) {
                            // Truncation at l = 4 is exact
                            auto lMax = std::min(degree, index_t{4});
                            auto j = 0;
                            for (int l = 0; l <= lMax; l += 2) {
                                for (int m = -l; m <= l; ++m) {
                                    // As result has shape (detX×detY)×#poses×#coeffs, the j-th
                                    // block corresponds to h_j, which is the same for all
                                    // pixels in the detector at a given pose, hence we fill
                                    // slice(pose)
                                    weights.getBlock(l * l + l + m)[idx] = coeffs[j];
                                    j++;
                                }
                            }

                        } else if (symmetry == Symmetry::even) {
                            const auto coeffCount = std::min(
                                SphericalCoefficientsDescriptor::coefficientCount(symmetry, degree),
                                index_t{15});
                            for (auto j = 0; j < coeffCount; ++j) {
                                weights.getBlock(j)[idx] = coeffs[j];
                            }
                        }
                        idx++;
                    }
                }
            }
        }

        return weights;
    }

    template <typename data_t>
    DataContainer<data_t> evaluateMalecki(const XGIDetectorDescriptor& detectorDescriptor,
                                          const DirVecList<data_t>& directions)
    {
        using namespace axdt;

        // obtain number of sampling directions, number of measured images
        const index_t numDirs = directions.size();
        const index_t poses = detectorDescriptor.getNumberOfCoefficientsPerDimension()[2];

        // set up the block descriptor
        auto directionSpace = IdenticalBlocksDescriptor{numDirs, detectorDescriptor};

        // Shape (detX×detY)×#poses×#dirs
        auto sampledWeightFunction = empty<data_t>(directionSpace);

        if (detectorDescriptor.isParallelBeam()) {

            for (index_t i = 0; i < numDirs; ++i) {
                // obtain sampling direction
                DirVec<data_t> e = directions[asUnsigned(i)];
                if (abs(e.norm() - 1) > 1e-5)
                    throw std::invalid_argument("direction vector at index " + std::to_string(i)
                                                + " not normalized");

#pragma omp parallel for
                for (index_t pose = 0; pose < poses; ++pose) {

                    // obtain geometry object for current image
                    const Geometry camera = detectorDescriptor.getGeometryAt(pose);

                    // allocate the grating's sensitivity vector
                    DirVec<data_t> t;

                    // allocate helper objects for ray computation
                    DirVec<data_t> l;

                    // sampling direction: set above, normalized by design

                    // beam direction: last column of rotation matrix (scaling is forced to be
                    // isotropic, z-direction/viewing axis)
                    // TODO: Why do we take the last row?
                    l = camera.getRotationMatrix().row(2).transpose().cast<data_t>();

                    // sensitivity direction: first column of rotation matrix (scaling is forced
                    // to be isotropic, x-direction/horizontal axis)
                    t = (camera.getRotationMatrix().transpose() * detectorDescriptor.getSensDir())
                            .cast<data_t>();

                    // compute the sensitivty function (|s × e|<e,t>)²
                    auto h = math::sq(l.cross(e).norm() * e.dot(t));

                    // h is the same for all pixels in the detector
                    // we can therefore fill the whole slice (corresponding to a slab of shape
                    // (detX×detY) with the same value
                    sampledWeightFunction.getBlock(i).slice(pose).fill(h);
                }
            }
        } else {

            const index_t px = detectorDescriptor.getNumberOfCoefficientsPerDimension()[0];
            const index_t py = detectorDescriptor.getNumberOfCoefficientsPerDimension()[1];

            for (index_t i = 0; i < numDirs; ++i) {
                // obtain sampling direction
                DirVec<data_t> e = directions[asUnsigned(i)];

                if (abs(e.norm() - 1) > 1e-5)
                    throw std::invalid_argument("direction vector at index " + std::to_string(i)
                                                + " not normalized");

#pragma omp parallel for
                for (index_t pose = 0; pose < poses; ++pose) {
                    // set index to base for currently processed image and direction
                    index_t idx = pose * px * py;

                    // obtain geometry object for current image
                    auto camera = detectorDescriptor.getGeometryAt(pose);

                    // allocate the grating's sensitivity vector
                    DirVec<data_t> t;

                    // allocate helper objects for ray computation
                    DirVec<data_t> s;
                    IndexVector_t pt(3);

                    // traverse image
                    for (index_t y = 0; y < py; ++y) {
                        for (index_t x = 0; x < px; ++x) {
                            // sampling direction: set above, normalized by design
                            // beam direction: get ray as provided by projection matrix
                            pt << x, y, pose;
                            auto ray = detectorDescriptor.computeRayFromDetectorCoord(pt);
                            s = ray.direction().cast<data_t>();

                            // sensitivity direction: first column of rotation matrix (scaling
                            // is forced to be isotropic, x-direction/horizontal axis)
                            t = (camera.getRotationMatrix().transpose()
                                 * detectorDescriptor.getSensDir())
                                    .cast<data_t>();

                            // compute the factor: (|sxe|<e,t>)^2
                            // apply the factor (the location is x/y/n, but there is no need to
                            // compute that)
                            const auto h = math::sq(s.cross(e).norm() * e.dot(t));

                            sampledWeightFunction.getBlock(i)[idx++] = h;
                        }
                    }
                }
            }
        }

        return sampledWeightFunction;
    }

    template <typename data_t>
    DataContainer<data_t>
        numericalWeightingFunction(const XGIDetectorDescriptor& detectorDescriptor,
                                   const Symmetry symmetry, const index_t degree,
                                   const DirVecList<data_t>& directions)
    {
        // Shape of sampled weighting function: (detX×detY)×#poses×#dirs
        auto sampledMalecki = evaluateMalecki(detectorDescriptor, directions);

        // Rescale in order as to obtain the correct normalization for the legacy tests
        sampledMalecki *= sqrt(4 * pi<data_t> / directions.size());

        // Result has shape (detX×detY)×#poses×#coeffs
        SphericalCoefficientsDescriptor coefficientDescriptor{detectorDescriptor, symmetry, degree};

        SphericalHarmonicsTransform<data_t> evaluate{coefficientDescriptor, directions};

        return evaluate.applyAdjoint(sampledMalecki);
    }

    template DataContainer<float> axdt::exactWeightingFunction(const XGIDetectorDescriptor&,
                                                               const Symmetry, const index_t);
    template DataContainer<double> axdt::exactWeightingFunction(const XGIDetectorDescriptor&,
                                                                const Symmetry, const index_t);

    template DataContainer<float> axdt::numericalWeightingFunction(const XGIDetectorDescriptor&,
                                                                   const Symmetry, const index_t,
                                                                   const DirVecList<float>&);

    template DataContainer<double> axdt::numericalWeightingFunction(const XGIDetectorDescriptor&,
                                                                    const Symmetry, const index_t,
                                                                    const DirVecList<double>&);

} // namespace elsa::axdt
