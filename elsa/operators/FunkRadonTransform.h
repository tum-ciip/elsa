#pragma once

#include "LinearOperator.h"
#include "DataContainer.h"
#include "SphericalCoefficientsDescriptor.h"
#include "elsaDefines.h"

namespace elsa::axdt
{

    template <typename data_t>
    Eigen::DiagonalMatrix<data_t, Eigen::Dynamic> funkRadonMatrix(const Symmetry symmetry,
                                                                  const index_t degree);

    template <typename data_t>
    class FunkRadonTransform : public LinearOperator<data_t>
    /**
     * @class FunkRadonTransform
     * @brief Represents the Funk Radon Transform in the spherical harmonics basis.
     *
     * The Funk-Radon transform is diagonal in the spherical harmonics basis (cf.
     * https://en.wikipedia.org/wiki/Funk_transform), so this operator just needs to calculate
     * the legendre polynomials at zero and scale each block of the spherical coefficients
     * data descriptor by its proper eigenvalue.
     * @author Cederik Höfs
     */
    {
    public:
        /**
         * @brief Constructs a FunkRadonTransform object with the specified descriptor.
         * @param descriptor The descriptor for range and domain of the transform.
         */
        explicit FunkRadonTransform(const SphericalCoefficientsDescriptor& descriptor);

        /**
         * @brief Default destructor.
         */
        ~FunkRadonTransform() override = default;

    protected:
        /**
         * @brief Default copy constructor, hidden from non-derived classes to prevent potential
         * slicing.
         * @param other The FunkRadonTransform object to copy.
         */
        FunkRadonTransform(const FunkRadonTransform<data_t>& other) = default;

        /**
         * @brief Applies the Funk Radon Transform to the input data container.
         * @param x The input data container.
         * @param Ax The output data container.
         */
        void applyImpl(const DataContainer<data_t>& x, DataContainer<data_t>& Ax) const override;

        /**
         * @brief Applies the adjoint of the Funk Radon Transform to the input data container.
         * @param y The input data container.
         * @param Aty The output data container.
         */
        void applyAdjointImpl(const DataContainer<data_t>& y,
                              DataContainer<data_t>& Aty) const override;

        /**
         * @brief Creates a clone of the FunkRadonTransform object.
         * @return A pointer to the cloned FunkRadonTransform object.
         */
        FunkRadonTransform<data_t>* cloneImpl() const override;

        /**
         * @brief Checks if the FunkRadonTransform object is equal to another LinearOperator object.
         * @param other The other LinearOperator object to compare.
         * @return True if the objects are equal, false otherwise.
         */
        bool isEqual(const LinearOperator<data_t>& other) const override;
    };

} // namespace elsa::axdt