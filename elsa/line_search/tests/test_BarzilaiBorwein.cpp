#include "doctest/doctest.h"

#include "BarzilaiBorwein.h"
#include "Identity.h"
#include "Logger.h"
#include "Scaling.h"
#include "VolumeDescriptor.h"
#include "LeastSquares.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("line_search");
TYPE_TO_STRING(BarzilaiBorwein<float>);
TYPE_TO_STRING(BarzilaiBorwein<double>);

template <template <typename> typename T, typename data_t>
constexpr data_t return_data_t(const T<data_t>&);

TEST_CASE_TEMPLATE("BarzilaiBorwein: Solving a simple problem", data_t, float, double)
{
    // Set seed for Eigen Matrices!
    srand((unsigned int) 666);

    // eliminate the timing info from console for the tests
    Logger::setLevel(Logger::LogLevel::OFF);

    GIVEN("a simple problem")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 2, 2;
        VolumeDescriptor dd(numCoeff);

        Eigen::Matrix<data_t, -1, 1> bVec(dd.getNumberOfCoefficients());
        bVec.setRandom();
        DataContainer dcB(dd, bVec);

        Vector_t<data_t> randomData(dd.getNumberOfCoefficients());
        randomData.setRandom();
        DataContainer<data_t> scaleFactors(dd, randomData * static_cast<data_t>(5.14));

        Scaling<data_t> scalingOp(scaleFactors);
        LeastSquares<data_t> prob(scalingOp, dcB);

        WHEN("setting up a BarzilaiBorwein Line Search and 0s for an initial guess")
        {
            BarzilaiBorwein<data_t> line_search{prob, 10, 1e-4, 0.1, 0.5};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 0;
                    std::array<data_t, 10> solutions;
                    if (std::string("d") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.0400000000000000077715611723760957829654216766357421875,
                            0.045384403305425945196471815279437578283250331878662109375,
                            0.049494203653845693369195402055993326939642429351806640625,
                            3.24379098151975941988212071009911596775054931640625,
                            3.33635941738023777958233040408231317996978759765625,
                            0.260222491161744728938032267251401208341121673583984375,
                            0.04815395073148805160645480327730183489620685577392578125,
                            0.045145496057139973544725108922648360021412372589111328125,
                            0.061778814978757014275689840587801882065832614898681640625,
                            1.288725548365815765095021561137400567531585693359375,
                        };
#else
                        solutions = {
                            0.0400000000000000077715611723760957829654216766357421875,
                            0.04538440330542593825757791137220920063555240631103515625,
                            0.04949420365384572806366492159213521517813205718994140625,
                            3.243790981519763416685009360662661492824554443359375,
                            3.336359417380237335493120554019697010517120361328125,
                            0.260222491161715085983274775571771897375583648681640625,
                            0.048153950731486087899479997531670960597693920135498046875,
                            0.0451454960571401608948605144178145565092563629150390625,
                            0.06177881497877447947164597508162842132151126861572265625,
                            1.288725548365809547846083660260774195194244384765625,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    } else if (std::string("f") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.0400000028312206268310546875, 0.0453844107687473297119140625,
                            0.0494942106306552886962890625, 3.2437889575958251953125,
                            3.3363592624664306640625,       0.2602055966854095458984375,
                            0.0481527037918567657470703125, 0.0451455228030681610107421875,
                            0.0617888979613780975341796875, 1.2887237071990966796875,
                        };
#else
                        solutions = {
                            0.0400000028312206268310546875, 0.0453844107687473297119140625,
                            0.0494942106306552886962890625, 3.24378871917724609375,
                            3.3363592624664306640625,       0.2602055966854095458984375,
                            0.0481527037918567657470703125, 0.0451455153524875640869140625,
                            0.0617888830602169036865234375, 1.288723468780517578125,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    }

                    for (index_t i = 0; i < solutions.size(); ++i) {
                        auto di = -prob.getGradient(xi);
                        data_t alpha = line_search.solve(xi, di);
                        CHECK_EQ(alpha - solutions[i], doctest::Approx(0));
                        xi += alpha * di;
                    }
                }
            }
        }

        WHEN("setting up BarzilaiBorwein line search and 1s for an intial guess")
        {
            BarzilaiBorwein<data_t> line_search{prob, 10, 1e-4, 0.1, 0.5};

            THEN("the clone works correctly")
            {
                auto lsClone = line_search.clone();

                CHECK_NE(lsClone.get(), &line_search);
                CHECK_EQ(*lsClone, line_search);

                AND_THEN("it works as expected")
                {
                    auto xi = DataContainer<data_t>(prob.getDomainDescriptor());
                    xi = 1;
                    std::array<data_t, 10> solutions;
                    if (std::string("d") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.0400000000000000077715611723760957829654216766357421875,
                            0.045238147717068639963589049557413090951740741729736328125,
                            0.04831767718081773910210330313930171541869640350341796875,
                            1.24828226829568933453629142604768276214599609375,
                            1.254733266097184607446024529053829610347747802734375,
                            0.11626521050440656590563293093509855680167675018310546875,
                            0.045202077469764935513385495369220734573900699615478515625,
                            0.045162689812112528919829657070295070298016071319580078125,
                            1.59142462101650483674575298209674656391143798828125,
                            6.002024203220798881375230848789215087890625,
                        };
#else
                        solutions = {
                            0.0400000000000000077715611723760957829654216766357421875,
                            0.045238147717068639963589049557413090951740741729736328125,
                            0.04831767718081773910210330313930171541869640350341796875,
                            1.24828226829568933453629142604768276214599609375,
                            1.254733266097184607446024529053829610347747802734375,
                            0.11626521050440653815005731530618504621088504791259765625,
                            0.045202077469764921635597687554763979278504848480224609375,
                            0.045162689812112521980935753163066692650318145751953125,
                            1.5914246210166691497533975052647292613983154296875,
                            6.0020242032207864468773550470359623432159423828125,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS

                    } else if (std::string("f") == typeid(data_t).name()) {
#ifdef ELSA_HAS_CUDA_PROJECTORS
                        solutions = {
                            0.0400000028312206268310546875, 0.0452381484210491180419921875,
                            0.048317693173885345458984375,  1.24828350543975830078125,
                            1.25473308563232421875,         0.11627618968486785888671875,
                            0.0452020466327667236328125,    0.0451626479625701904296875,
                            1.59144842624664306640625,      6.00201892852783203125,
                        };
#else
                        solutions = {
                            0.0400000028312206268310546875, 0.0452381484210491180419921875,
                            0.048317693173885345458984375,  1.24828350543975830078125,
                            1.25473308563232421875,         0.1162761747837066650390625,
                            0.0452020466327667236328125,    0.04516263306140899658203125,
                            1.59135913848876953125,         6.00203609466552734375,
                        };
#endif // ELSA_HAS_CUDA_PROJECTORS
                    }

                    for (index_t i = 0; i < solutions.size(); ++i) {
                        auto di = -prob.getGradient(xi);
                        data_t alpha = line_search.solve(xi, di);
                        CHECK_EQ(alpha - solutions[i], doctest::Approx(0));
                        xi += alpha * di;
                    }
                }
            }
        }
    }
}
TEST_SUITE_END();
