#pragma once

#include "elsaDefines.h"

namespace elsa
{

    template <typename data_t>
    data_t cubic_interpolation(data_t x0, data_t f0, data_t der_f0, data_t x1, data_t f1, data_t x2,
                               data_t f2);
}
