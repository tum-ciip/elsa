#include "ExpLeastSquares.h"

#include "DataContainer.h"
#include "DataDescriptor.h"
#include "LinearOperator.h"
#include "TypeCasts.hpp"
#include "Scaling.h"

namespace elsa
{
    template <typename data_t>
    ExpLeastSquares<data_t>::ExpLeastSquares(const LinearOperator<data_t>& A,
                                             const DataContainer<data_t>& b)
        : Functional<data_t>(A.getDomainDescriptor()), A_(A.clone()), b_(b)
    {
    }

    template <typename data_t>
    bool ExpLeastSquares<data_t>::isDifferentiable() const
    {
        return true;
    }

    template <typename data_t>
    const LinearOperator<data_t>& ExpLeastSquares<data_t>::getOperator() const
    {
        return *A_;
    }

    template <typename data_t>
    const DataContainer<data_t>& ExpLeastSquares<data_t>::getDataVector() const
    {
        return b_;
    }

    template <typename data_t>
    data_t ExpLeastSquares<data_t>::evaluateImpl(const DataContainer<data_t>& x) const
    {
        auto d = exp(-A_->apply(x));
        d -= b_;
        return 0.5 * d.squaredL2Norm();
    }

    template <typename data_t>
    void ExpLeastSquares<data_t>::getGradientImpl(const DataContainer<data_t>& x,
                                                  DataContainer<data_t>& out) const
    {
        auto d = exp(-A_->apply(x));
        out = -A_->applyAdjoint((d - b_) * d);
    }

    template <typename data_t>
    LinearOperator<data_t>
        ExpLeastSquares<data_t>::getHessianImpl(const DataContainer<data_t>& x) const
    {
        auto d = exp(-A_->apply(x));
        return adjoint(*A_) * (d * (2.f * d - b_)) * *A_;
    }

    template <typename data_t>
    ExpLeastSquares<data_t>* ExpLeastSquares<data_t>::cloneImpl() const
    {
        return new ExpLeastSquares(*A_, b_);
    }

    template <typename data_t>
    bool ExpLeastSquares<data_t>::isEqual(const Functional<data_t>& other) const
    {
        if (!Functional<data_t>::isEqual(other))
            return false;

        auto fn = downcast_safe<ExpLeastSquares<data_t>>(&other);
        return fn && *A_ == *fn->A_ && b_ == fn->b_;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class ExpLeastSquares<float>;
    template class ExpLeastSquares<double>;
} // namespace elsa
