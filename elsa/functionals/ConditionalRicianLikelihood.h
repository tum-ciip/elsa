#pragma once

#include "DataContainer.h"
#include "Functional.h"

namespace elsa
{
    // Split into WeightedL2!

    /**
     * @brief This functional calcualtes the likelihood of measuring the mean intensity a and
     * amplitude b in a dark-field imaging setup, given the spherical harmonics coefficients of the
     * scattering function eta, assuming the Rician noise model. The domain of this functional
     * is described by a SphericalCoefficientsDescriptor.
     *
     *  @author Cederik Höfs
     * @tparam data_t The data type of the functional.
     */
    template <typename data_t>
    class ConditionalRicianLikelihood : public Functional<data_t>
    {
    public:
        /**
         * @brief Constructs a ConditionalRicianLikelihood object.
         *
         * @param ffa The flat-field measured value of RV a.
         * @param ffb The flat-field measured value of RV b.
         * @param a The measured value of RV a.
         * @param b The measured value of RV b.
         * @param axdt_op The operator modeling the projection of the dark-field signal.
         * @param N The total grating-stepping steps.
         * @param approximate Flag indicating whether to use a Gaussian distribution to approximate
         * the Rician.
         */
        ConditionalRicianLikelihood(const DataContainer<data_t>& ffa,
                                    const DataContainer<data_t>& ffb,
                                    const DataContainer<data_t>& a, const DataContainer<data_t>& b,
                                    const LinearOperator<data_t>& axdt_op, index_t N,
                                    bool approximate);

        /// make copy constructor deletion explicit
        ConditionalRicianLikelihood(const ConditionalRicianLikelihood<data_t>&) = delete;

        /// default destructor
        ~ConditionalRicianLikelihood() override = default;

        /**
         * @brief Checks if the functional is differentiable.
         *
         * @return True, as this functional is differentiable.
         */
        bool isDifferentiable() const override;

    protected:
        /**
         * @brief Calculates the likelihood, either by using the Rician distribution or by
         * approximating it with a normal distribution.
         *
         * @param Rx The input data container.
         * @return The evaluation result.
         */
        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        /**
         * @brief Computes the gradient of the functional.
         *
         * @param Rx The input data container.
         * @param out The output data container to store the gradient.
         */
        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>& out) const override;

        /**
         * @brief Computes the Hessian of the functional.
         *
         * @param Rx The input data container.
         * @return The computed Hessian as a linear operator.
         */
        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        /**
         * @brief Creates a clone of the functional.
         *
         * @return A pointer to the cloned functional.
         */
        ConditionalRicianLikelihood<data_t>* cloneImpl() const override;

        /**
         * @brief Compares the functional with another functional.
         *
         * @param other The other functional to compare with.
         * @return True if the functionals are equal, false otherwise.
         */
        bool isEqual(const Functional<data_t>& other) const override;

    private:
        /// the flat-field measured value of RV a
        DataContainer<data_t> flatFieldA;

        /// the flat-field measured value of RV b
        DataContainer<data_t> flatFieldB;

        /// the measured value of RV a
        DataContainer<data_t> sampleA;

        /// the measured value of RV b
        DataContainer<data_t> sampleB;

        /// operator modeling the projection of the dark-field signal
        std::unique_ptr<LinearOperator<data_t>> axdt_op_;

        /// store the frequently used value alpha (= ffb / ffa)
        DataContainer<data_t> flatFieldVisibility;

        /// Store quotient of b and d (as this is constant given a,ffa,ffb)
        DataContainer<data_t> a_alpha;

        /// total grating-stepping steps
        data_t N_;

        /// bool to indicate whether to use a Gaussian distribution to approximate the Racian
        bool approximate_;
    };

} // namespace elsa
