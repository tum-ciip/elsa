#include "ConditionalRicianLikelihood.h"
#include "DataContainer.h"
#include "TypeCasts.hpp"
#include "LinearOperator.h"
#include "BlockLinearOperator.h"
#include "Timer.h"
#include "Scaling.h"

namespace elsa
{

    template <typename data_t>
    ConditionalRicianLikelihood<data_t>::ConditionalRicianLikelihood(
        const DataContainer<data_t>& ffa, const DataContainer<data_t>& ffb,
        const DataContainer<data_t>& a, const DataContainer<data_t>& b,
        const LinearOperator<data_t>& axdt_op, index_t N, bool approximate)
        : Functional<data_t>{axdt_op.getDomainDescriptor()},
          flatFieldA{ffa},
          flatFieldB{ffb},
          sampleA{a},
          sampleB{b},
          axdt_op_{axdt_op.clone()},
          flatFieldVisibility{ffb / ffa},
          a_alpha{a * flatFieldVisibility},
          N_{as<data_t>(N)},
          approximate_{approximate}
    {
    }

    template <typename data_t>
    bool ConditionalRicianLikelihood<data_t>::isDifferentiable() const
    {
        return true;
    }

    template <typename data_t>
    data_t ConditionalRicianLikelihood<data_t>::evaluateImpl(const DataContainer<data_t>& eta) const
    {
        Timer timeguard("AXDTStatRecon", "evaluate");

        auto log_d = -axdt_op_->apply(eta);
        auto d = exp(log_d);

        if (approximate_) {
            // Simply weighted exponential east squares (on b)...
            return 0.5 * (sq(sampleB - a_alpha * d) / sampleA).sum();
        } else {
            auto alpha_d = flatFieldVisibility * d;
            return N_ / 4 * (sampleA * sq(alpha_d)).sum()
                   - bessel_log_0((N_ / 2) * sampleB * alpha_d).sum();
        }
    }

    template <typename data_t>
    void ConditionalRicianLikelihood<data_t>::getGradientImpl(const DataContainer<data_t>& eta,
                                                              DataContainer<data_t>& out) const
    {
        Timer timeguard("AXDTStatRecon", "getGradient");

        const auto log_d = -axdt_op_->apply(eta);
        const auto d = exp(log_d);

        const auto alpha_d = flatFieldVisibility * d;

        auto tmp_a_alpha_d = sampleA * alpha_d; // Might reuse this later in approximation codepath

        if (approximate_) {
            auto tmp = sampleB - tmp_a_alpha_d;
            tmp *= alpha_d;

            axdt_op_->applyAdjoint(tmp, out);
        } else {
            auto tmp = sampleA * sq(alpha_d);
            tmp *= -N_ * 0.5f;
            auto z = sampleB * alpha_d;
            z *= N_ * 0.5f;
            tmp += z * bessel_1_0(z);

            return axdt_op_->applyAdjoint(tmp, out);
        }
    }

    template <typename data_t>
    LinearOperator<data_t>
        ConditionalRicianLikelihood<data_t>::getHessianImpl(const DataContainer<data_t>& eta) const
    {
        Timer timeguard("AXDTStatRecon", "getHessian");

        const auto d = exp(-axdt_op_->apply(eta));

        const auto alpha_d = flatFieldVisibility * d;

        const auto H_2_2 = [&]() {
            if (approximate_) {
                return alpha_d * (2 * sampleA * alpha_d - sampleB);
            } else {
                auto tmp = N_ * sampleA * sq(alpha_d);
                auto z = (N_ * 0.5) * sampleB * alpha_d;
                auto quot_z = bessel_1_0(z);
                tmp += sq(z) * (sq(quot_z) - data_t{1});
                return tmp;
            }
        }();

        return adjoint(*axdt_op_) * H_2_2 * *axdt_op_;
    }

    template <typename data_t>
    ConditionalRicianLikelihood<data_t>* ConditionalRicianLikelihood<data_t>::cloneImpl() const
    {
        return new ConditionalRicianLikelihood(flatFieldA, flatFieldB, sampleA, sampleB, *axdt_op_,
                                               as<index_t>(N_), approximate_);
    }

    template <typename data_t>
    bool ConditionalRicianLikelihood<data_t>::isEqual(const Functional<data_t>& other) const
    {
        if (!Functional<data_t>::isEqual(other))
            return false;

        auto otherFn = downcast_safe<ConditionalRicianLikelihood>(&other);
        if (!otherFn)
            return false;

        if (otherFn->approximate_ != approximate_)
            return false;

        return (otherFn->flatFieldA == flatFieldA || otherFn->flatFieldB == flatFieldB
                || otherFn->sampleA == sampleA || otherFn->sampleB == sampleB
                || *(otherFn->axdt_op_) == *(axdt_op_) || otherFn->N_ == N_);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class ConditionalRicianLikelihood<float>;
    template class ConditionalRicianLikelihood<double>;
} // namespace elsa
