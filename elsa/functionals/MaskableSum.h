#pragma once

#include "BlockDescriptor.h"
#include "DataContainer.h"
#include "Functional.h"
#include "IdenticalBlocksDescriptor.h"
#include "RandomBlocksDescriptor.h"
#include "TypeCasts.hpp"
#include "elsaDefines.h"
#include <algorithm>
#include <memory>

namespace elsa
{

    /// Represents sum of Functionals f_i whose evaluation can be restricted to a mask/set S
    ///
    /**
     * @brief Functional representing a sum of functionals,
     * enabling selective evaluation of the resulting sum
     * @f[ f(x) = \Sum_{i\in S} f_i(x) @f]
     * as well as its gradient
     * @f[ \nabla f(x) = \Sum_{i\in S} \nabla f_i(x) @f]
     *
     * @tparam data_t data type for the domain of the residual of the functional, defaulting to
     * real_t
     *
     * @author
     * * Cederik Höfs - initial code
     *
     */
    template <typename data_t = real_t>
    class MaskableSum final : public Functional<data_t>
    {

    public:
        /**
         * @brief Construct a new Maskable Sum object from summands
         *
         * @param fns List of wrapped Functionals
         * @param mask Optional initialization of the mask
         */
        explicit MaskableSum(std::vector<std::unique_ptr<Functional<data_t>>> fns,
                             std::optional<std::vector<bool>> mask = std::nullopt);

        // Make copy constructor deletion explicit.
        explicit MaskableSum(const MaskableSum&) = delete;

        size_t numFunctions() const;

        void setMask(const std::vector<bool>& mask);
        void setAll();

        // Useful for mergeProblems/introspection...
        const std::vector<std::unique_ptr<Functional<data_t>>>& getFunctions() const;
        const std::vector<bool>& getMask() const;

    private:
        std::vector<bool> mask;
        std::vector<std::unique_ptr<Functional<data_t>>> functions;

        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        /// The derivative of the sum of functions is the sum of the derivatives
        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>& out) const override;

        /// The Hessian of the sum of functions is the sum of the Hessians
        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        /// Polymorphic clone implementations
        MaskableSum<data_t>* cloneImpl() const override;

        /// Polymorphic equality implementations
        bool isEqual(const Functional<data_t>& other) const override;
    };
} // namespace elsa
