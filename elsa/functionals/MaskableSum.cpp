#include "MaskableSum.h"
#include "DataContainer.h"
#include "DataDescriptor.h"
#include "Functional.h"
#include "TypeCasts.hpp"

#include <memory>
#include <numeric>
#include <vector>

namespace elsa
{
    template <typename data_t>
    MaskableSum<data_t>::MaskableSum(std::vector<std::unique_ptr<Functional<data_t>>> fs,
                                     std::optional<std::vector<bool>> mask)
        : Functional<data_t>(fs.at(0)->getDomainDescriptor()),
          mask{mask.value_or(std::vector<bool>(fs.size(), true))},
          functions(std::move(fs))
    {

        if (functions.empty()) {
            // Unreachable, Functional{} fails first
            throw Error{"MaskableSum: Must contain at least one Functional!"};
        }

        if (mask.has_value() && mask->size() != functions.size()) {
            // Unreachable, Functional{} fails first
            throw Error{"MaskableSum: Mask size must equal number of Functionals!"};
        }

        auto& firstDesc = functions.front()->getDomainDescriptor();

        // Determine if all descriptors are equal
        bool allEqual = std::all_of(functions.begin(), functions.end(), [&](const auto& x) {
            return x->getDomainDescriptor() == firstDesc;
        });

        if (!allEqual) {
            throw Error{"MaskableSum: Functionals must all have identical domains!"};
        }
    }

    template <typename data_t>
    void MaskableSum<data_t>::setMask(const std::vector<bool>& mask)
    {
        if (mask.size() != functions.size()) {
            throw Error{"MaskableSum: setMask with invalid mask size"};
        }
        this->mask = mask;
    }

    template <typename data_t>
    const std::vector<bool>& MaskableSum<data_t>::getMask() const
    {
        return mask;
    }

    template <typename data_t>
    const std::vector<std::unique_ptr<Functional<data_t>>>&
        MaskableSum<data_t>::getFunctions() const
    {
        return functions;
    }

    template <typename data_t>
    void MaskableSum<data_t>::setAll()
    {
        std::fill(mask.begin(), mask.end(), true);
    }

    template <typename data_t>
    size_t MaskableSum<data_t>::numFunctions() const
    {
        return functions.size();
    }

    template <typename data_t>
    data_t MaskableSum<data_t>::evaluateImpl(const DataContainer<data_t>& Rx) const
    {
        if (Rx.getDataDescriptor() != this->getDomainDescriptor()) {
            throw Error{"MaskableSum: Descriptor of argument is unexpected"};
        }

        return std::transform_reduce(functions.begin(), functions.end(), mask.begin(), data_t{0},
                                     std::plus<>{},
                                     [&](auto& fn, auto m) { return m ? fn->evaluate(Rx) : 0; });
    }

    template <typename data_t>
    void MaskableSum<data_t>::getGradientImpl(const DataContainer<data_t>& Rx,
                                              DataContainer<data_t>& out) const
    {
        if (Rx.getDataDescriptor() != this->getDomainDescriptor()) {
            throw Error("MaskableSum: Descriptor of argument is unexpected");
        }

        out.fill(0);

        auto grad = emptylike(Rx);
        for (std::size_t i = 0; i < functions.size(); ++i) {
            if (mask[i]) {
                functions[i]->getGradient(Rx, grad);
                out += grad;
            }
        }
    }

    template <typename data_t>
    LinearOperator<data_t>
        MaskableSum<data_t>::getHessianImpl(const DataContainer<data_t>& Rx) const
    {
        if (Rx.getDataDescriptor() != this->getDomainDescriptor()) {
            throw Error("MaskableSum: Descriptor of argument is unexpected");
        }
        LinearOperator<data_t> H{this->getDomainDescriptor(), this->getDomainDescriptor()};
        for (std::size_t i = 0; i < functions.size(); ++i) {
            if (mask[i]) {
                H = H + functions[i]->getHessian(Rx);
            }
        }
        return H;
    }

    template <typename data_t>
    MaskableSum<data_t>* MaskableSum<data_t>::cloneImpl() const
    {
        std::vector<std::unique_ptr<Functional<data_t>>> copies;
        copies.reserve(functions.size());
        for (const auto& ptr : functions) {
            copies.push_back(ptr->clone());
        }

        return new MaskableSum<data_t>(std::move(copies), mask);
    }

    template <typename data_t>
    bool MaskableSum<data_t>::isEqual(const Functional<data_t>& other) const
    {
        if (!Functional<data_t>::isEqual(other)) {
            return false;
        }

        const auto& fn = downcast<const MaskableSum<data_t>>(other);

        if (mask != fn.mask) {
            return false;
        }

        return std::equal(functions.begin(), functions.end(), fn.functions.begin(),
                          [](const auto& l, const auto& r) { return (*l) == (*r); });
    }

    // ------------------------------------------
    // explicit template instantiation
    template class MaskableSum<float>;
    template class MaskableSum<double>;
    template class MaskableSum<complex<float>>;
    template class MaskableSum<complex<double>>;

} // namespace elsa
