#include "doctest/doctest.h"
#include "NdView.h"

#include <thrust/universal_vector.h>
#include <thrust/copy.h>

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("core");

template <typename T, mr::StorageType tag>
NdViewTagged<T, mr::StorageType::universal>
    recreate_with_different_strides(const NdViewTagged<T, tag>& in_view, IndexVector_t new_strides)
{
    size_t size = 1;
    const IndexVector_t& shape = in_view.shape();
    const IndexVector_t& strides = in_view.strides();
    for (size_t i = 0; i < static_cast<size_t>(shape.size()); i++) {
        if (new_strides(i) < 0)
            throw std::runtime_error("This test cannot handle negative strides");
        size += (shape(i) - 1) * new_strides(i);
    }

    struct Cleanup {
        thrust::universal_vector<T>* _data;

        void operator()() { delete _data; }
    } cleanup;
    thrust::universal_vector<T>* data = new thrust::universal_vector<T>(size);
    cleanup._data = data;

    NdViewTagged<T, mr::StorageType::universal> out(cleanup._data->data().get(), shape, new_strides,
                                                    cleanup);
    auto out_begin = out.range().begin();
    for (auto& e : in_view.range()) {
        *(out_begin++) = e;
    }
    return out;
}

TEST_CASE("NdView")
{
    using View = NdViewTagged<int, mr::StorageType::universal>;

    thrust::universal_vector<int> data(27);
    for (size_t i = 0; i < 27; ++i)
        data[i] = i;

    int* raw_data = thrust::raw_pointer_cast(data.data());
    IndexVector_t strides(3), shape(3);
    strides << 1, 3, 9;
    shape << 3, 3, 3;

    SUBCASE("NdView cleanup")
    {
        size_t cleanupCount = 0;
        {
            View view(raw_data, shape, strides, [&]() { ++cleanupCount; });
        }
        CHECK_EQ(cleanupCount, 1);
    }

    SUBCASE("NdView cleanup - fix")
    {
        size_t cleanupCount = 0;
        {
            View view(raw_data, shape, strides, [&]() { ++cleanupCount; });

            View view2 = view.fix(1, 1);
        }
        CHECK_EQ(cleanupCount, 1);
    }

    SUBCASE("NdView cleanup - slice")
    {
        size_t cleanupCount = 0;

        {
            View view(raw_data, shape, strides, [&]() { ++cleanupCount; });

            View view2 = view.slice(1, 1, 2);
        }
        CHECK_EQ(cleanupCount, 1);
    }

    SUBCASE("3D 3x3x3 valid contiguous, ordered")
    {
        {
            View view(raw_data, shape, strides, []() {});

            CHECK(view.is_contiguous());
            CHECK(view.is_canonical());
            CHECK_EQ(view.size(), 27);

            auto cont = view.contiguous_range();
            auto ord = view.canonical_range();
            auto str = view.range();
            auto contIt = cont.begin();
            auto ordIt = ord.begin();
            auto strIt = str.begin();
            for (size_t i = 0; i < data.size(); ++i) {
                CHECK_EQ(*contIt, data[i]);
                CHECK_EQ(*ordIt, data[i]);
                CHECK_EQ(*strIt, data[i]);

                ++contIt;
                ++ordIt;
                ++strIt;
            }
            CHECK_EQ(contIt, cont.end());
            CHECK_EQ(ordIt, ord.end());
            CHECK_EQ(strIt, str.end());
        }
    }

    SUBCASE("3D 3x3x3 - fix of third dimension")
    {
        View view(raw_data, shape, strides, []() {});

        View fix = view.fix(2, 1);

        CHECK_EQ(fix.shape().size(), 2);
        CHECK(fix.is_contiguous());
        CHECK(fix.is_canonical());
        CHECK_EQ(fix.size(), 9);
        CHECK_EQ(fix.shape()[0], 3);
        CHECK_EQ(fix.shape()[1], 3);

        /*
         *  09 12 15
         *  10 13 16
         *  11 14 17
         */
        auto range = fix.range();
        auto it = range.begin();
        auto expect = {9, 10, 11, 12, 13, 14, 15, 16, 17};
        for (size_t i = 0; i < expect.size(); ++i) {
            CHECK_EQ(*it, *(expect.begin() + i));
            ++it;
        }
        CHECK_EQ(it, range.end());
    }

    SUBCASE("3D 3x3x3 - fix of second dimension")
    {
        View view(raw_data, shape, strides, []() {});

        View fix = view.fix(1, 1);

        CHECK_EQ(fix.shape().size(), 2);
        CHECK_FALSE(fix.is_contiguous());
        CHECK_FALSE(fix.is_canonical());
        CHECK_EQ(fix.size(), 9);
        CHECK_EQ(fix.shape()[0], 3);
        CHECK_EQ(fix.shape()[1], 3);

        /*
         *  03 12 21
         *  04 13 22
         *  05 14 23
         */
        auto range = fix.range();
        auto it = range.begin();
        auto expect = {3, 4, 5, 12, 13, 14, 21, 22, 23};
        for (size_t i = 0; i < expect.size(); ++i) {
            CHECK_EQ(*it, *(expect.begin() + i));
            ++it;
        }
        CHECK_EQ(it, range.end());
    }

    SUBCASE("3D 3x3x3 - fix of first dimension")
    {
        View view(raw_data, shape, strides, []() {});

        View fix = view.fix(0, 1);

        CHECK_EQ(fix.shape().size(), 2);
        CHECK_FALSE(fix.is_contiguous());
        CHECK_FALSE(fix.is_canonical());
        CHECK_EQ(fix.size(), 9);
        CHECK_EQ(fix.shape()[0], 3);
        CHECK_EQ(fix.shape()[1], 3);

        /*
         *  01 10 19
         *  04 13 22
         *  07 16 25
         */
        auto range = fix.range();
        auto it = range.begin();
        auto expect = {1, 4, 7, 10, 13, 16, 19, 22, 25};
        for (size_t i = 0; i < expect.size(); ++i) {
            CHECK_EQ(*it, *(expect.begin() + i));
            ++it;
        }
        CHECK_EQ(it, range.end());
    }

    SUBCASE("3D 3x3x3 - fix all dimensions")
    {
        View view(raw_data, shape, strides, []() {});

        View fix = view.fix(0, 1).fix(0, 1).fix(0, 1);

        CHECK(fix.is_contiguous());
        CHECK(fix.is_canonical());
        CHECK_EQ(fix.size(), 1);
        CHECK_EQ(fix.shape().size(), 0);
        CHECK_EQ(*fix.canonical_range().begin(), 13);
        CHECK_EQ(fix.canonical_range().begin() + 1, fix.canonical_range().end());
    }

    SUBCASE("3D 3x3x3 - invalid fix")
    {
        View view(raw_data, shape, strides, []() {});

        CHECK_THROWS(view.fix(3, 0));
        CHECK_THROWS(view.fix(0, 3));
    }

    SUBCASE("3D 3x3x3 - slice third dimension")
    {
        View view(raw_data, shape, strides, []() {});

        View fix = view.slice(2, 0, 2);

        CHECK_EQ(fix.shape().size(), 3);
        CHECK(fix.is_contiguous());
        CHECK(fix.is_canonical());
        CHECK_EQ(fix.size(), 18);
        CHECK_EQ(fix.shape()[0], 3);
        CHECK_EQ(fix.shape()[1], 3);
        CHECK_EQ(fix.shape()[2], 2);

        auto range = fix.range();
        auto it = range.begin();
        auto expect = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};
        for (size_t i = 0; i < expect.size(); ++i) {
            CHECK_EQ(*it, *(expect.begin() + i));
            ++it;
        }
        CHECK_EQ(it, range.end());
    }

    SUBCASE("3D 3x3x3 - slice second dimension")
    {
        View view(raw_data, shape, strides, []() {});

        View fix = view.slice(1, 0, 2);

        CHECK_EQ(fix.shape().size(), 3);
        CHECK(!fix.is_contiguous());
        CHECK(!fix.is_canonical());
        CHECK_EQ(fix.size(), 18);
        CHECK_EQ(fix.shape()[0], 3);
        CHECK_EQ(fix.shape()[1], 2);
        CHECK_EQ(fix.shape()[2], 3);

        auto range = fix.range();
        auto it = range.begin();
        auto expect = {0, 1, 2, 3, 4, 5, 9, 10, 11, 12, 13, 14, 18, 19, 20, 21, 22, 23};
        for (size_t i = 0; i < expect.size(); ++i) {
            CHECK_EQ(*it, *(expect.begin() + i));
            ++it;
        }
        CHECK_EQ(it, range.end());
    }

    SUBCASE("3D 3x3x3 - slice first dimension")
    {
        View view(raw_data, shape, strides, []() {});

        View fix = view.slice(0, 0, 2);

        CHECK_EQ(fix.shape().size(), 3);
        CHECK(!fix.is_contiguous());
        CHECK(!fix.is_canonical());
        CHECK_EQ(fix.size(), 18);
        CHECK_EQ(fix.shape()[0], 2);
        CHECK_EQ(fix.shape()[1], 3);
        CHECK_EQ(fix.shape()[2], 3);

        auto range = fix.range();
        auto it = range.begin();
        auto expect = {0, 1, 3, 4, 6, 7, 9, 10, 12, 13, 15, 16, 18, 19, 21, 22, 24, 25};
        for (size_t i = 0; i < expect.size(); ++i) {
            CHECK_EQ(*it, *(expect.begin() + i));
            ++it;
        }
        CHECK_EQ(it, range.end());
    }

    SUBCASE("3D 3x3x3 - slice all dimensions")
    {
        View view(raw_data, shape, strides, []() {});

        View fix = view.slice(0, 0, 2).slice(1, 0, 2).slice(2, 0, 2);

        CHECK_EQ(fix.shape().size(), 3);
        CHECK(!fix.is_contiguous());
        CHECK(!fix.is_canonical());
        CHECK_EQ(fix.size(), 8);
        CHECK_EQ(fix.shape()[0], 2);
        CHECK_EQ(fix.shape()[1], 2);
        CHECK_EQ(fix.shape()[2], 2);

        auto range = fix.range();
        auto it = range.begin();
        auto expect = {0, 1, 3, 4, 9, 10, 12, 13};
        for (size_t i = 0; i < expect.size(); ++i) {
            CHECK_EQ(*it, *(expect.begin() + i));
            ++it;
        }
        CHECK_EQ(it, range.end());
    }

    SUBCASE("3D 3x3x3 - invalid slice")
    {
        View view(raw_data, shape, strides, []() {});

        CHECK_THROWS(view.slice(0, 1, 0));
        CHECK_THROWS(view.slice(3, 1, 2));
        CHECK_THROWS(view.slice(1, 0, 4));
    }

    SUBCASE("3D indexing")
    {
        GIVEN("A view and a view with reversed strides")
        {
            View view(raw_data, shape, strides, []() {});
            View transpose_view(raw_data, shape, strides.reverse(), []() {});
            THEN("Indexing the view is equivalent to indexing the transposed view with reversed "
                 "index order")
            {
                for (index_t i = 0; i < shape(0); i++) {
                    for (index_t j = 0; j < shape(1); j++) {
                        for (index_t k = 0; k < shape(2); k++) {
                            CHECK_EQ(view(i, j, k), transpose_view(k, j, i));
                        }
                    }
                }
            }
        }

        GIVEN("A view with neither row, nor column major layout")
        {
            IndexVector_t strides(3);
            strides << 3, 1, 9;
            View view(raw_data, shape, strides, []() {});
            THEN("Indexing in column major (rather depth major) order is equivalent to iteration "
                 "over range()")
            {
                auto range = view.range();
                auto it = range.begin();
                for (index_t k = 0; k < shape(2); k++) {
                    for (index_t j = 0; j < shape(1); j++) {
                        for (index_t i = 0; i < shape(0); i++) {
                            CHECK_NE(it, range.end());
                            CHECK_EQ(view(i, j, k), *(it++));
                        }
                    }
                }
            }

            THEN("Undersupplying indices throws an exception")
            {
                CHECK_THROWS(view(1, 2));
            }

            THEN("Oversupplying indices throws an exception")
            {
                CHECK_THROWS(view(1, 2, 3, 4));
            }

            THEN("Accessing out of bounds throws an exception")
            {
                CHECK_THROWS(view(1, 12, 3));
            }

            THEN("Creating an ordered range from non-ordered data throws an exception")
            {
                REQUIRE(!view.is_canonical());
                CHECK_THROWS(view.canonical_range());
            }
        }
    }

    SUBCASE("Contiguous range exception")
    {
        GIVEN("A view with non-contiguous strides")
        {
            IndexVector_t shape(1);
            shape << 3;
            IndexVector_t strides(1);
            strides << 2;
            View view(raw_data, shape, strides, []() {});

            REQUIRE(!view.is_contiguous());
            CHECK_THROWS(view.contiguous_range());
        }
    }

    SUBCASE("Arithmetic")
    {
        /* defining new test data to avoid divide by zero*/
        thrust::universal_vector<int> data(27);
        int* raw_data = thrust::raw_pointer_cast(data.data());

#define TEST_BINOP(op)                                                     \
    WHEN("Using operator " #op)                                            \
    {                                                                      \
        TEST_BINOP_WITH_STRIDES(op, strides, strides);                     \
        TEST_BINOP_WITH_STRIDES(op, strides.reverse(), strides.reverse()); \
        TEST_BINOP_WITH_STRIDES(op, strides.reverse(), strides);           \
        TEST_BINOP_WITH_STRIDES(op, strides, strides.reverse());           \
    }

#define TEST_BINOP_WITH_STRIDES(op, strides1, strides2)                         \
    {                                                                           \
        for (size_t i = 0; i < 27; ++i)                                         \
            data[i] = i + 1;                                                    \
        View left(raw_data, shape, (strides1), []() {});                        \
        View right(raw_data, shape, (strides2), []() {});                       \
        auto combined = left op right;                                          \
                                                                                \
        auto left_range = left.range();                                         \
        auto right_range = right.range();                                       \
        auto combined_range = combined.range();                                 \
        auto combined_it = combined_range.begin();                              \
        for (auto left_it = left_range.begin(), right_it = right_range.begin(); \
             left_it != left_range.end() && right_it != right_range.end()       \
             && combined_it != combined_range.end();                            \
             left_it++, right_it++, combined_it++) {                            \
            REQUIRE_EQ(*left_it op* right_it, *combined_it);                    \
        }                                                                       \
    }

        TEST_BINOP(+);
        TEST_BINOP(-);
        TEST_BINOP(*);
        TEST_BINOP(/);
        TEST_BINOP(%);
        TEST_BINOP(<);
        TEST_BINOP(>);
        TEST_BINOP(<=);
        TEST_BINOP(>=);
        TEST_BINOP(==);
        TEST_BINOP(!=);
        TEST_BINOP(&);
        TEST_BINOP(|);
        TEST_BINOP(&&);
        TEST_BINOP(||);
#undef TEST_BINOP
#undef TEST_BINOP_WITH_STRIDES
    }

    SUBCASE("Boolean Indexing")
    {
        auto test_helper_assign_zero = [&](const IndexVector_t& strides1,
                                           const IndexVector_t& strides2) {
            View canonical_view(thrust::raw_pointer_cast(data.data()), shape, strides, []() {});
            View view1 = recreate_with_different_strides(canonical_view, strides1);
            View view2 = recreate_with_different_strides(canonical_view, strides2);
            view2[view1 > 9 && view1 <= 18] = 0;

            auto it = view2.range().begin();
            for (auto element : canonical_view.range()) {
                REQUIRE_EQ(element > 9 && element <= 18 ? 0 : element, *(it++));
            }
        };

        GIVEN("Equally layed out indexed and index expressions and a constant right side")
        {
            test_helper_assign_zero(strides, strides);

            /* non-canonically layed out*/
            test_helper_assign_zero(strides.reverse(), strides.reverse());

            /* non-contiguous */
            test_helper_assign_zero(strides * 2, strides * 2);
        }

        GIVEN("Differently layed out indexed and index expressions and a constant right side")
        {
            /* non-canonically layed out*/
            test_helper_assign_zero(strides, strides.reverse());

            test_helper_assign_zero(strides.reverse(), strides);

            /* non-contiguous */
            test_helper_assign_zero(strides, strides * 2);

            test_helper_assign_zero(strides * 2, strides);

            test_helper_assign_zero(strides.reverse(), strides * 2);

            test_helper_assign_zero(strides * 2, strides.reverse());
        }

        auto test_helper_assign_doubled = [&](const IndexVector_t& strides1,
                                              const IndexVector_t& strides2,
                                              const IndexVector_t& strides3) {
            View canonical_view(thrust::raw_pointer_cast(data.data()), shape, strides, []() {});
            View view1 = recreate_with_different_strides(canonical_view, strides1);
            View view2 = recreate_with_different_strides(canonical_view, strides2);
            View view3 = recreate_with_different_strides(canonical_view, strides3);
            view2[view1 > 9 && view1 <= 18] = view3 * 2;

            auto it = view2.range().begin();
            for (auto element : canonical_view.range()) {
                REQUIRE_EQ(element > 9 && element <= 18 ? element * 2 : element, *(it++));
            }
        };

        GIVEN("Equally layed out indexed, index and right side expressions")
        {
            test_helper_assign_doubled(strides, strides, strides);

            /* non-canonically layed out*/
            test_helper_assign_doubled(strides.reverse(), strides.reverse(), strides.reverse());

            /* non-contiguous */
            test_helper_assign_doubled(strides * 2, strides * 2, strides * 2);
        }

        GIVEN("Differently layed out indexed, index and right side expressions")
        {
            /* non-canonically layed out*/
            test_helper_assign_doubled(strides, strides.reverse(), strides.reverse());
            test_helper_assign_doubled(strides.reverse(), strides, strides.reverse());
            test_helper_assign_doubled(strides.reverse(), strides.reverse(), strides);
            test_helper_assign_doubled(strides.reverse(), strides, strides);
            test_helper_assign_doubled(strides, strides.reverse(), strides);
            test_helper_assign_doubled(strides, strides, strides.reverse());

            /* non-contiguous */
            test_helper_assign_doubled(strides, strides * 2, strides * 3);
            test_helper_assign_doubled(strides * 2, strides, strides * 3);
            test_helper_assign_doubled(strides * 2, strides * 3, strides);
            test_helper_assign_doubled(strides * 2, strides, strides);
            test_helper_assign_doubled(strides, strides * 2, strides);
            test_helper_assign_doubled(strides, strides, strides * 2);

            test_helper_assign_doubled(strides, strides * 2, strides.reverse());
            test_helper_assign_doubled(strides * 2, strides, strides.reverse());
            test_helper_assign_doubled(strides * 2, strides.reverse(), strides);
        }
    }
}

TEST_SUITE_END();
