#include "doctest/doctest.h"
#include "StridedIterator.h"

#include <thrust/universal_vector.h>
#include <thrust/copy.h>
#include <thrust/execution_policy.h>

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("core");

TEST_CASE("StridedIterator")
{
    thrust::universal_vector<int> data(8);
    data[0] = 10;
    data[1] = 20;
    data[2] = 30;
    data[3] = 40;
    data[4] = 50;
    data[5] = 60;
    data[6] = 70;
    data[7] = 80;

    thrust::universal_vector<DimData<std::ptrdiff_t>> dimData2d(2);
    dimData2d[0] = {2, 1};
    dimData2d[1] = {4, 2};
    StridedRange strided2d(data.begin(), 8, dimData2d.data().get(), 2);

    GIVEN("a strided 2D range with column major order")
    {
        auto stridedIt = strided2d.begin();
        auto stridedItEnd = strided2d.end();
        THEN("the iteration order is the same as the contiguous layout")
        {
            for (int i : data) {
                CHECK_EQ(i, *stridedIt);
                ++stridedIt;
            }
            CHECK_EQ(stridedIt, stridedItEnd);
        }
    }

    thrust::universal_vector<DimData<std::ptrdiff_t>> dimData2dTranspose(2);

    dimData2dTranspose[0] = {4, 2};
    dimData2dTranspose[1] = {2, 1};
    StridedRange strided2dTranspose(data.begin(), 8, dimData2dTranspose.data().get(), 2);

    GIVEN("a strided 2D range with row major order (1) and one with colum major order (2)")
    {
        THEN("the iteration order of (2) corresponds to the transpose of (1)")
        {
            for (size_t row = 0; row < 4; row++) {
                for (size_t col = 0; col < 2; col++) {
                    CHECK_EQ(*(strided2d.begin() + (row * 2 + col)),
                             *(strided2dTranspose.begin() + (col * 4 + row)));
                }
            }
        }
    }

    thrust::universal_vector<DimData<ptrdiff_t>> dimData3d(3);
    dimData3d[0] = {2, 1};
    dimData3d[1] = {2, 2};
    dimData3d[2] = {2, 4};
    StridedRange strided3d(data.begin(), 8, dimData3d.data().get(), 3);

    thrust::universal_vector<DimData<ptrdiff_t>> dimData3dTranspose(3);
    dimData3dTranspose[0] = {2, 4};
    dimData3dTranspose[1] = {2, 2};
    dimData3dTranspose[2] = {2, 1};
    StridedRange strided3dTranspose(data.begin(), 8, dimData3dTranspose.data().get(), 3);

    GIVEN("a strided 3D range with row major order (1) and one with depth major order (2)")
    {
        THEN(
            "the iteration order of (2) corresponds to the iteration over (1) with flipped indices")
        {
            for (size_t row = 0; row < 2; row++) {
                for (size_t col = 0; col < 2; col++) {
                    for (size_t depth = 0; depth < 2; depth++) {
                        CHECK_EQ(*(strided3d.begin() + (row * 4 + col * 2 + depth)),
                                 *(strided3dTranspose.begin() + (depth * 4 + col * 2 + row)));
                    }
                }
            }
        }
    }

    GIVEN("a strided 3D range and a universal_vector")
    {
        thrust::universal_vector<int> dst(8);
        THEN("the range can be copied on the device")
        {
            /* check that strided iterators can be used in device code without faulting */
            thrust::copy(thrust::device, strided3d.begin(), strided3d.end(), dst.begin());

            for (size_t i = 0; i < 8; i++) {
                CHECK_EQ(dst[i], data[i]);
            }
        }
    }
}

TEST_SUITE_END();
