#pragma once
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/functional.h>
#include <thrust/fill.h>
#include <thrust/copy.h>
#include <iostream>

namespace elsa
{
    template <typename diff_t>
    struct DimData {
        diff_t shape{0};
        diff_t stride{0};
    };

    /// @brief Iterable range, in which elements traversed in canonical order
    /// @tparam Iterator
    /// @see elsa::NdView::is_canonical()
    template <typename Iterator>
    class StridedRange
    {
    public:
        typedef typename thrust::iterator_difference<Iterator>::type difference_type;

        struct StrideFunctor : public thrust::unary_function<difference_type, difference_type> {
            size_t _dimCount{0};
            DimData<difference_type>* _dimData{nullptr};

            StrideFunctor(DimData<difference_type>* dimData, size_t dimCount)
                : _dimCount{dimCount}, _dimData{dimData}
            {
            }

            ~StrideFunctor() = default;

            __host__ __device__ difference_type operator()(const difference_type& i) const
            {
                /* special case for zero-dimensional volumes */
                if (_dimCount == 0)
                    return i;

                difference_type out = 0;
                difference_type alongDim = i;
                for (size_t dim = 0; dim < _dimCount; dim++) {
                    out += (alongDim % _dimData[dim].shape) * _dimData[dim].stride;
                    alongDim = alongDim / _dimData[dim].shape;
                }
                return out;
            }
        };

        typedef typename thrust::counting_iterator<difference_type> CountingIterator;
        typedef typename thrust::transform_iterator<StrideFunctor, CountingIterator>
            TransformIterator;
        typedef typename thrust::permutation_iterator<Iterator, TransformIterator>
            PermutationIterator;

        typedef PermutationIterator iterator;

        /// @param first     iterator to the first element of the range
        /// @param count     number of elements in the range
        /// @param dimData   non-owning pointer to the dimensions and stride data.
        ///                  Make sure the strided range does outlive this pointer!
        ///                  If the Iterator type is a device iterator, dimData must be
        ///                  accessible on the device!
        /// @param dimCount  dimensionality of the data
        StridedRange(Iterator first, difference_type count, DimData<difference_type>* dimData,
                     size_t dimCount)
            : _first{first}, _count{count}, _dimCount{dimCount}, _dimData{dimData}
        {
        }

        iterator begin()
        {
            return PermutationIterator(
                _first, TransformIterator(CountingIterator(0), StrideFunctor(_dimData, _dimCount)));
        }

        iterator end() { return begin() + _count; }

        iterator begin() const
        {
            return PermutationIterator(
                _first, TransformIterator(CountingIterator(0), StrideFunctor(_dimData, _dimCount)));
        }

        iterator end() const { return begin() + _count; }

    protected:
        Iterator _first;
        difference_type _count;
        size_t _dimCount;
        DimData<difference_type>* _dimData;
    };
} // namespace elsa
