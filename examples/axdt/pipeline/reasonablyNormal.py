#!/usr/bin/env python3
import argparse

from pathlib import Path

import yaml

import numpy as np
import matplotlib.pyplot as plt

from utils import *
from skimage.draw import disk


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='AXDT Normality Diagnosis')

	parser.add_argument("input", type=Path, help="Input directory")
	parser.add_argument("--config", "-c", type=Path,
						default="dataset_dci.yaml", help="Name of YAML config file")

	parser.add_argument("--period", "-p", type=int,
						default=8, help="Grating periods")

	args = parser.parse_args()

	# Use all available data
	args.numerical = False
	args.binning = 1

	with open(args.input/args.config, "r") as f:
		args.yaml = yaml.safe_load(f)


	folder = args.input
	binning_factor = args.binning
	config = args.yaml

	# path to _a_ <-- corresponds to absorption
	a_path = folder / config["data"]["a-file"]
	# path to _b_ <-- dark-field can be computed form here
	b_path = folder / config["data"]["b-file"]
	# path to reference/flat field a
	ffa_path = folder / config["data"]["ffa-file"]
	# path to reference/flat field b
	ffb_path = folder / config["data"]["ffb-file"]

	a = preprocess(elsa.EDF.readf(str(a_path.absolute())), binning_factor)
	b = preprocess(elsa.EDF.readf(str(b_path.absolute())), binning_factor)
	# ffa = preprocess(elsa.EDF.readf(str(ffa_path.absolute())), binning_factor)
	# ffb = preprocess(elsa.EDF.readf(str(ffb_path.absolute())), binning_factor)

	a = np.asarray(a)
	b = np.asarray(b)

	sx, sy = a.shape[1:]

	# Mask again to ignore masked out values

	radius = min(sx, sy) * 0.35

	center = np.array(a[0].shape) // 2
	rr, cc = disk(center, radius, shape=a[0].shape)

	nu = 0.5 * b[:, rr, cc]
	sigma = np.sqrt(a[:, rr, cc] / (2*args.period))

	r = nu/sigma

    plt.xlabel(r"$\frac{\nu}{\sigma}$")
    plt.hist(r.flatten(), bins=200, density=True, log=True)
    plt.show()
