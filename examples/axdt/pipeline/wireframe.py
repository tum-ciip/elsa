#!/usr/bin/env python3
import argparse
from typing import List
import matplotlib.pyplot as plt
import numpy as np

from pyelsa.axdt import generate_half_sphere
from utils import *


def generateTest(coeffs: List[int]) -> np.ndarray:
    """
    Generate a test spherical coefficient field for visualization
    Parameters
    ----------
    coeffs : List[int]
        The spherical harmonics to synthesize
    """
    eta = np.zeros((15, len(coeffs), 1, 1))

    # Reproduces clover-like shape of |Y_2^{\pm 2}|
    for i, c in enumerate(coeffs):
        eta[c, i, 0, 0] = 1.0

    return eta


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='AXDT Fiber Extraction Sampling Directions')

    parser.add_argument("--phi", type=int, default=30,
                        help="Resolution in phi")
    parser.add_argument("--theta", type=int, default=15,
                        help="Resolution in theta")

    args = parser.parse_args()

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(projection="3d")

    dirs, E = generate_half_sphere(args.theta, args.phi)

    ax.scatter(*np.array(dirs).T, color='red')

    for (i, j) in E:
        ax.plot([dirs[i][0], dirs[j][0]], [dirs[i][1], dirs[j][1]], [
            dirs[i][2], dirs[j][2]], color='black')

    ax.scatter([-1, 1], [-1, 1], [-1, 1], color="k")

    set_axes_equal(ax)
    ax.set_box_aspect([1, 1, 1])

    plt.show()
