#!/usr/bin/env python3
import argparse
import itertools
from pathlib import Path
from typing import Tuple

import matplotlib as mpl

import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from utils import *

from matplotlib import cbook

import pyelsa as elsa

mplstyle.use('fast')


def visualizeOrientationDensity(ax: Axes3D, eta: np.ndarray, zScore: float = .5, res: Tuple[int, int] = (20, 20), truncL: int = 4, color='b') -> None:

    theta_res, phi_res = res

    print(f"Using (θ×φ)={theta_res}×{phi_res} sampling directions...")

    # Create full sphere
    phi = np.linspace(0, 2 * np.pi, res[1])
    theta = np.linspace(0, np.pi, res[0])
    x = np.outer(np.cos(phi), np.sin(theta))
    y = np.outer(np.sin(phi), np.sin(theta))
    z = np.outer(np.ones(np.size(phi)), np.cos(theta))

    directions = list(
        np.stack([x.flatten(), y.flatten(), z.flatten()], axis=1))

    etaDC = elsa.axdt.makeEtaContainer(eta, truncL)

    funkRadon = elsa.axdt.FunkRadonTransform(etaDC.getDataDescriptor())
    evaluate = elsa.axdt.SphericalHarmonicsTransform(
        etaDC.getDataDescriptor(), directions)

    transformedCoeffs = funkRadon.apply(etaDC)
    orientationDensity = evaluate.apply(transformedCoeffs)

    r = np.asarray(orientationDensity)

    r = abs(r)
    r /= np.max(r)
    r /= 2  # Divide by two as distance between to surfaces is one

    # Decide whether to plot a surface based on its maximal value and the relation to the statistics of the maximal values
    rMax = np.max(r, axis=0)
    sigma_r = np.std(rMax)
    threshold = np.mean(rMax) + zScore * sigma_r

    idxes = np.where(rMax > threshold)

    # Gruesome index magic to index r with the incomplete result of np.where
    r = (r.T[idxes[::-1]]).T

    mag = r.reshape(
        (phi_res, theta_res, len(idxes[0])))

    print(f"Plotting {len(idxes[0])} surfaces...")

    iz, iy, ix = idxes

    xs = ix+x[..., np.newaxis]*mag
    ys = iy+y[..., np.newaxis]*mag
    zs = iz+z[..., np.newaxis]*mag

    for i in range(len(ix)):
        ax.plot_surface(xs[..., i], ys[..., i],
                        zs[..., i], color=color, alpha=0.5)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Surface Visualization')

    parser.add_argument("--eta", "-c", type=Path,
                        help="Dark field signal coefficients to visualize")

    parser.add_argument("--output", "-o", type=Path,
                        help="Path to store visualization at")

    parser.add_argument("--interactive", "-i", action="store_true",
                        help="Show interactive plot")

    parser.add_argument("--every", "-e", type=int, default=1,
                        help="Plot surfaces for every nth voxel")

    parser.add_argument("--phi", type=int, default=20,
                        help="Resolution in phi")
    parser.add_argument("--theta", type=int, default=10,
                        help="Resolution in theta")

    parser.add_argument("--zscore", "-z", type=float, default=0.5,
                        help="Number of standard deviations to consider for fiber thresholding")

    parser.add_argument("--max-l", "-l", type=int, default=4,
                        help="Truncate degrees above this l")

    args = parser.parse_args()

    if args.max_l is not None and args.max_l % 2 != 0:
        print("Truncation l must be even!")
        sys.exit(1)

    if args.output is None and not args.interactive:
        print("Need to supply output directory or --interactive!")
        sys.exit(1)

    efiles = [args.eta]

    fig = plt.figure(figsize=(10, 10), constrained_layout=True)

    ax = fig.add_subplot(projection="3d")

    for eta_file in efiles:

        print(f"Processing {eta_file}...")

        eta = np.load(eta_file)[:, ::args.every, ::args.every, ::args.every]

        visualizeOrientationDensity(
            ax, eta, args.zscore, (args.theta, args.phi), args.max_l, "b")

        lx, ly, lz = eta.shape[1:]

        ax.scatter([0], [0], [0], color="k")
        ax.scatter([lx], [ly], [lz], color="k")

        set_axes_equal(ax)
        ax.set_box_aspect([1, 1, 1])

        fig.suptitle(f"AXDT scattering surface visualization - {eta_file}")
        if args.output is not None:
            fig.savefig(args.output)

        if not args.interactive:
            plt.close(fig)

    if args.interactive:
        plt.show()
