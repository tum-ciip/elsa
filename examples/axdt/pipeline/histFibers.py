#!/usr/bin/env python3
import argparse
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from utils import *


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Loss Calculator')

    parser.add_argument("eta", type=Path,
                        help="Spherical harmonics coefficients")

    parser.add_argument("--max-l", type=int, default=4,
                        help="Maximum l to plot")

    parser.add_argument("--zscore", "-z", type=float, default=0.5,
                        help="Number of standard deviations to consider for fiber thresholding")

    parser.add_argument("--phi", type=int, default=30,
                        help="Resolution in phi")
    parser.add_argument("--theta", type=int, default=15,
                        help="Resolution in theta")

    parser.add_argument("--no-clip", action="store_true",
                        help="Don't clip scattering strength before Funk-Radon transform")

    args = parser.parse_args()

    # Load the scattering functions
    eta = np.load(args.eta)

    magnitudes, indices, directions = extractFibers(
        eta, 1, (args.theta, args.phi), args.max_l, not args.no_clip)

    fig, ax = plt.subplots()

    fibers = directions[indices]
    fibers = fibers.reshape(-1, 3)

    phi = np.arctan2(fibers[:, 1], fibers[:, 0]) * 180 / np.pi
    theta = np.arccos(fibers[:, 2]) * 180 / np.pi

    ax.hist2d(theta, phi, density=True, bins=(args.theta, args.phi))

    ax.set_xlabel("θ")
    ax.set_ylabel("φ")
    ax.set_title(f"Histogram of local maxima of η in polar coordinates")
    plt.show()
