#!/usr/bin/env python3
import argparse
from pathlib import Path

import numpy as np
from pyelsa.pyelsa.axdt import generate_half_sphere
from utils import *

import pyelsa as elsa

silence()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Loss Calculator')

    parser.add_argument("eta0", type=Path,
                        help="Ground truth spherical harmonics coefficients")
    parser.add_argument("eta1", type=Path,
                        help="Reconstructed spherical harmonics coefficients")

    parser.add_argument("--mu0", type=Path,
                        help="Ground truth attenuation data (for modes 2a and 2b)")
    parser.add_argument("--mu1", type=Path,
                        help="Reconstructed attenuation data (for modes 2a and 2b)")

    parser.add_argument("--phi", type=int, default=20,
                        help="Resolution in phi")
    parser.add_argument("--theta", type=int, default=20,
                        help="Resolution in theta")

    parser.add_argument("--max-l", "-l", type=int, default=4)

    args = parser.parse_args()

    # Load the scattering functions
    eta0 = np.load(args.eta0)
    eta1 = np.load(args.eta1)

    assert (eta0.shape == eta1.shape)

    if args.mu0 is not None:
        mu0 = np.load(args.mu0)
        mu1 = np.load(args.mu1)

        assert (mu0.shape == mu1.shape)

    _, sz, sy, sx = eta0.shape

    phi = np.linspace(0, 2 * np.pi, res[1])
    theta = np.linspace(0, np.pi, res[0])
    x = np.outer(np.cos(phi), np.sin(theta))
    y = np.outer(np.sin(phi), np.sin(theta))
    z = np.outer(np.ones(np.size(phi)), np.cos(theta))

    directions = list(
        np.stack([x.flatten(), y.flatten(), z.flatten()], axis=1))

    eta0DC = makeEtaContainer(eta0, args.max_l)
    eta1DC = makeEtaContainer(eta1, args.max_l)

    evaluate = elsa.axdt.SphericalHarmonicsTransform(
        eta0DC.getDataDescriptor(), directions)

    scatterDiff = evaluate.apply(eta0DC-eta1DC)
    print(
        f"L2(scatterDiff)/L2(scatterGround): {scatterDiff.l2Norm()/eta0DC.l2Norm()}")
    print(
        f"L2(coeffDiff)/L2(coeffGround): {np.sqrt(np.sum((eta0-eta1)**2)/np.sum(eta0**2))}")
