#!/usr/bin/env python3

import matplotlib.pyplot as plt

import numpy as np
from utils import *

import pyelsa as elsa

numWeights = np.load("data/oldAna.npy")
anaWeights = np.load("data/oldNum.npy")

numWeights /= np.max(numWeights)
anaWeights /= np.max(anaWeights)

diff = numWeights - anaWeights

ls = [0, 2, 4]
count = sum(2*l+1 for l in ls)

fig, axs = plt.subplots(nrows=count, ncols=1, sharex=True,
                        sharey=False, constrained_layout=True, figsize=(8, 11))

i = 0
for l in ls:
    for m in range(-l, l+1):
        axs[i].set_ylabel(f"$h_{{{l}{m}}}$")
        axs[i].set_yscale("log")

        axs[i].plot(abs(numWeights[i]), color='r')
        axs[i].plot(abs(anaWeights[i]), color='b')
        axs[i].plot(diff[i], color='g')

        i += 1

axs[-1].plot([], [], label='numerical', color='r')
axs[-1].plot([], [], label='analytical', color='b')
axs[-1].plot([], [], label='difference', color='g')

axs[-1].set_xlabel("$j$")

fig.legend()

plt.tight_layout()
plt.show()

# print(f"min={np.min(diff):.3g}", f"max={np.max(diff):.3g}",
#       f"{np.mean(diff):.3g}±{np.std(diff):.3g}")

# print(np.correlate(numWeights[1], anaWeights[1]))

# # numWeights = numWeights.reshape((60, 300))
# # anaWeights = anaWeights.reshape((60, 300))

# fig, axs = plt.subplots(nrows=3, ncols=1, sharex=True, sharey=True)

# axs[0].imshow(numWeights)
# axs[1].imshow(anaWeights)
# axs[2].imshow(numWeights - anaWeights)
# plt.show()
