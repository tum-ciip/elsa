#!/usr/bin/env python3
import itertools
from utils import *
from matplotlib.colors import hsv_to_rgb
import numpy as np
import argparse
from pathlib import Path

import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Line3DCollection


mplstyle.use('fast')


def direction2color3D(v: np.ndarray) -> np.ndarray:
    """
    Arbirarily map a 3d direction to a color
    """

    x, y, z = v

    # Confine to lower half circle to color sticks green/blue
    if y > 0:
        x, y, z = -x, -y, z

    # Deliberately switch y and x in arctan2 to match colour wheel
    hue = np.arctan2(x, y) / np.pi
    if hue < 0:
        hue = 1 + hue

    # Make in-plane vectors more saturated
    saturation = 1-abs(z)
    return hsv_to_rgb([hue, saturation, 1.0])


def visualizeFibers3D(ax: Axes3D, magnitudes: np.ndarray, directionIndices: np.ndarray, samplingDirections: np.ndarray, zScore: float = .5) -> None:
    """
    Visualize the fibers in 3D
    Parameters
    ----------
    ax : Axes3D
        The axis to plot on
    magnitudes : np.ndarray
        The scattering strengths of the fibers
    directionIndices : np.ndarray
        The indices of the directions of the local maxima
    samplingDirections : np.ndarray
        The directions of the fibers
    numFibers : int
        The number of fibers to plot
    zScore : float
        The number of standard deviations to consider for fiber thresholding
    """

    threshold = np.mean(magnitudes) + zScore * np.std(magnitudes)

    idxes = np.where(magnitudes > threshold)

    print(f"Thresholding reduced average to: {len(
        idxes[0])/np.prod(magnitudes.shape[1:]):.2f}")

    magnitudes /= np.max(magnitudes)

    lines = []
    colors = []

    for fiber, iz, iy, ix in zip(*idxes):

        radius = magnitudes[fiber, iz, iy, ix]

        direction = samplingDirections[directionIndices[fiber, iz, iy, ix]]

        dx, dy, dz = direction * radius

        lines.append([(ix-dx, iy-dy, iz-dz), (ix+dx, iy+dy, iz+dz)])
        colors.append(direction2color3D(direction))

    line_collection = Line3DCollection(lines, colors=colors)
    ax.add_collection3d(line_collection)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='AXDT 3D Fibers Visualization')

    parser.add_argument("--input", "-i", required=True, type=Path,
                        help="Fiber file to visualize")

    parser.add_argument("--output", "-o", type=Path,
                        help="Path to store visualization at")

    parser.add_argument("--no-show", action="store_true",
                        help="Don't show the visualization interactively")

    parser.add_argument("--num", "-n", type=int, default=1,
                        help="Number of local maxima to plot")

    parser.add_argument("--max-l", "-l", type=int, default=4,
                        help="Truncate degrees above this l")

    parser.add_argument("--zscore", "-z", type=float, default=0.5,
                        help="Number of standard deviations to consider for fiber thresholding")

    parser.add_argument("--phi", type=int, default=30,
                        help="Resolution in phi")
    parser.add_argument("--theta", type=int, default=15,
                        help="Resolution in theta")

    parser.add_argument("--no-clip", action="store_true",
                        help="Don't clip scattering strength before Funk-Radon transform")

    parser.add_argument("--batch", action="store_true",
                        help="Process a directory of .npy files")

    args = parser.parse_args()

    if args.output is None and args.no_show:
        print("Need to supply output directory or omit no-show!")
        sys.exit(1)

    if args.batch:
        if args.input is None or not args.input.is_dir():
            print("Batch mode needs directory of .npy files!")
            sys.exit(1)
        if args.output is not None and not args.output.is_dir():
            print("Batch mode needs directory to write visualizations to")
            sys.exit(1)

        fiberfiles = args.input.glob("reco_eta_*.npy")
    else:
        fiberfiles = [args.input]

    for ffile in fiberfiles:

        if args.batch and (args.output/(ffile.stem + ".svg")).exists():
            print(f"Skipping {ffile}...")
            continue

        print(f"Processing {ffile}...")

        # Load the spherical coefficients
        data = np.load(ffile)

        # Extract fibers using a C++ helper function
        magnitudes, indices, directions = data["mag"], data["ind"], data["dir"]

        if args.num > magnitudes.shape[0]:
            print(
                f"WARNING: Requested top {args.num} fibers, but only {magnitudes.shape[0]} available in file")
            sys.exit(1)

        magnitudes = magnitudes[:args.num]
        indices = indices[:args.num]

        fig = plt.figure(figsize=(10, 10), constrained_layout=True)

        ax = fig.add_subplot(projection="3d")

        # Start in a sensible camera position
        ax.view_init(elev=90, azim=90, roll=0)

        visualizeFibers3D(ax, magnitudes, indices,
                          directions, args.zscore)

        # Use black dots to mark the origin and the end of the volume
        lx, ly, lz = magnitudes.shape[1:]

        ax.scatter([0], [0], [0], color="k")
        ax.scatter([lx], [ly], [lz], color="k")

        # Keep equal aspect ratio to not distort the view
        set_axes_equal(ax)
        ax.set_box_aspect([1, 1, 1])

        fig.suptitle(f"AXDT fiber visualization - {args.input}")
        if args.output is not None:
            if args.batch:
                fig.savefig(args.output/(ffile.stem + ".svg"))
            else:
                fig.savefig(args.output)

        if args.no_show:
            plt.close(fig)

    if not args.no_show:
        plt.show()
