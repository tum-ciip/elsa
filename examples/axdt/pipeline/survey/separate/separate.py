#!/usr/bin/env python3
import argparse
import csv
from pathlib import Path
from typing import Any, List, Optional, Tuple

import numpy as np
import yaml
from tqdm import trange
from timeit import default_timer as timer
from utils import *

import pyelsa as elsa

silence()


def arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='AXDT Reconstruction')

    parser.add_argument(
        "input", type=Path, help="Input directory")
    parser.add_argument("--config", "-c", type=Path,
                        default="dataset_dci.yaml", help="Name of YAML config file")

    parser.add_argument("--reco-dir", "-o", required=True, type=Path,
                        help="Path to store reconstruction at")

    parser.add_argument("--model", "-m,", required=True, type=str, choices=[
                        "0", "1", "3a", "3b", "0'"], help="Noise model(s): \n\t0 -> Gaussian log(d), \n\t1 -> Gaussian d, \n\t3a~ Rician b (approximated by Gaussian), \n\t3b ~ and Rician b")

    args = parser.parse_args()

    args.binning = 2
    args.period = 8
    args.numerical = False
    args.iters = 2001

    with open(args.input/args.config, "r") as f:
        args.yaml = yaml.safe_load(f)

    return args


def main():
    args = arguments()
    givenSetup = loadData(args)

    loss = setupProblem(args.model, *givenSetup, args.period)

    lambdas = [1e-10]

    solvers = [elsa.FGM(loss, elsa.FixedStepSize(loss, l)) for l in lambdas]
    names = ["FGM-fixed" for _ in lambdas]

    writeLoss = set(range(0, 501, 10)) | set(range(500, 2001, 50))
    writeImage = set(range(100, args.iters, 200))

    args.algo = "FGM-BB"

    logfile = args.reco_dir / \
        filename("log", args, args.model, args.iters, ".csv")

    with open(logfile, 'w', newline='') as csvfile:

        header = ["dataset", "iteration", "model",
                  "solver", "stepsize", "loss", "time"]
        writer = csv.DictWriter(
            csvfile, header, dialect="excel", extrasaction="raise")

        writer.writeheader()

        for name, solver, l in zip(names, solvers, lambdas):

            args.algo = name

            row = {"dataset": "crossed_sticks", "model": args.model,
                   "solver": name, "stepsize": l}

            start = timer()
            r = trange(args.iters, desc=name)

            x = solver.setup()

            for i in r:
                if i in writeImage:
                    save(args, args.model, i, x, "reco")

                if i in writeLoss:

                    row["iteration"] = i
                    row["time"] = timer()-start
                    row["loss"] = loss.evaluate(x)

                    writer.writerow(row)
                    csvfile.flush()

                x = solver.step(x)


if __name__ == '__main__':
    main()
