#!/usr/bin/env python3

import pyelsa as elsa
from utils import *
import numpy as np
import argparse
from pathlib import Path


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='AXDT 3D Fiber Extraction')

    parser.add_argument("--eta", "-c", required=True, type=Path,
                        help="Dark field signal coefficients")

    parser.add_argument("--output", "-o", type=Path,
                        help="Path to store fiber file at")

    parser.add_argument("--num", "-n", type=int, default=1,
                        help="Number of local maxima to extract")

    parser.add_argument("--max-l", "-l", type=int, default=4,
                        help="Truncate degrees above this l")

    parser.add_argument("--phi", type=float, default=2.0,
                        help="Angular resultion in phi")
    parser.add_argument("--theta", type=float, default=2.0,
                        help="Angular resolution in theta")

    parser.add_argument("--no-clip", action="store_true",
                        help="Don't clip scattering strength before Funk-Radon transform")

    parser.add_argument("--compress", action="store_true",
                        help="Save compressed")

    args = parser.parse_args()

    print(f"Processing {args.eta}...")

    # Load the spherical coefficients
    eta = np.load(args.eta)

    # As we only sample the upper half-sphere
    theta_res = 90 / args.theta
    phi_res = 360 / args.phi

    # Extract fibers using a C++ helper function
    magnitudes, indices, directions = elsa.axdt.extractFibers(
        eta, args.num, (theta_res, phi_res), args.max_l, not args.no_clip)

    print(f"Saving fibers to {args.output}...")
    # Save the fibers to disk
    if args.compress:
        np.savez_compressed(args.output, mag=magnitudes,
                            ind=indices, dir=directions)

    else:
        np.savez(args.output, mag=magnitudes,
                 ind=indices, dir=directions)
