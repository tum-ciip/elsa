#!/usr/bin/env python3
import argparse

from pathlib import Path

import yaml

import numpy as np
import matplotlib.pyplot as plt

from utils import *
from skimage.draw import disk


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Histogram Diff')

    parser.add_argument("input", type=Path,
                        help="Ground Truth Input directory")
    parser.add_argument("--config", "-c", type=Path,
                        default="dataset_dci.yaml", help="Name of YAML config file")

    parser.add_argument("--period", "-p", type=int,
                        default=8, help="Grating periods")

    parser.add_argument("--eta", type=Path, nargs="+",
                        help="Reconstructed images")

    parser.add_argument("--bins", type=int, default=200,
                        help="Number of bins")

    parser.add_argument("--log", action="store_true",
                        help="Logarithmic scale")

    args = parser.parse_args()

    # Use all available data
    args.numerical = False
    args.binning = 2

    with open(args.input/args.config, "r") as f:
        args.yaml = yaml.safe_load(f)

    axdt_op, absorp_op, ffa, ffb, a, b = loadData(args)

    matplotlib.rcParams.update({'font.size': 15})

    d = b * ffa / (a * ffb)

    plt.xlabel(r"$-\ln{d}$")
    plt.hist(-np.log(d).flatten(), bins=args.bins, density=True,
             log=args.log, alpha=0.8, label="Measured Data")

    if args.eta:
        for path in args.eta:
            eta = np.load(path)

            dc = makeEtaContainer(eta, 4)

            print(dc.shape)

            d = elsa.exp(-axdt_op.apply(dc))

            plt.hist(-np.log(d).flatten(), bins=args.bins, density=True,
                     log=args.log, alpha=0.5, label=path)

    plt.legend()

#     plt.savefig("nu_sigma_hist.svg", bbox_inches='tight')
    plt.show()
