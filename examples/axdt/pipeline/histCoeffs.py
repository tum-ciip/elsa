#!/usr/bin/env python3
from icosphere import icosphere
import argparse

from pathlib import Path
import re
from typing import List, Optional, Tuple

from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
import matplotlib.figure
import matplotlib.pyplot as plt
from networkx import density
import numpy as np
import mpl_toolkits
import tqdm

from utils import *

import pyelsa as elsa


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Loss Calculator')

    parser.add_argument("etas", type=Path, nargs="+",
                        help="Spherical harmonics coefficients")

    args = parser.parse_args()

    # Load the scattering functions
    etas = [np.load(e) for e in args.etas]

    num = 15

    fig, axs = plt.subplots(nrows=num, ncols=1, sharex=True,
                            sharey=False, constrained_layout=True)

    if num == 1:
        axs = [axs]

    for i in range(num):
        for j, e in enumerate(etas):
            axs[i].hist(e[i].flatten(), bins=100,
                        density=True, stacked=True, log=True, label=f"{args.etas[j]}", alpha=0.7)
        axs[i].legend()
    plt.show()
