#!/usr/bin/env python3
from icosphere import icosphere
import argparse

from pathlib import Path
import re
from typing import List, Optional, Tuple

from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
import matplotlib.figure
import matplotlib.pyplot as plt
from networkx import density
import numpy as np
import mpl_toolkits
import tqdm

from utils import *

import pyelsa as elsa


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Loss Calculator')

    parser.add_argument("etas", type=Path, nargs="+",
                        help="Spherical harmonics coefficients")

    parser.add_argument("--max-l", type=int, default=4,
                        help="Maximum l to plot")

    parser.add_argument("--frt", action="store_true",
                        help="Apply Funk-Radon transform")

    parser.add_argument("--log", action="store_true",
                        help="Logarithmic scale")

    args = parser.parse_args()

    # Load the scattering functions
    etas = [np.load(e) for e in args.etas]

    etaDCs = [makeEtaContainer(e, args.max_l) for e in etas]

    if args.frt:
        frt = elsa.axdt.FunkRadonTransform(etaDCs[0].getDataDescriptor())
        etaDCs = [frt.apply(e) for e in etaDCs]

    Q = elsa.axdt.SphericalHarmonicsTransform(
        etaDCs[0].getDataDescriptor(), sampling_directions)

    evals = [np.asarray(Q.apply(e)) for e in etaDCs]

    fig, ax = plt.subplots()

    for i, e in enumerate(evals):
        ax.hist(e.flatten(), bins=100,
                density=True, log=args.log, label=f"{args.etas[i]}", alpha=0.7)
    ax.legend()
    ax.set_title(
        f"Histogram of η evaluated on {len(sampling_directions)} directions {"(Funk-Radon transformed)" if args.frt else ""}")
    plt.show()
