#!/usr/bin/env python3
import argparse
from pathlib import Path
from typing import List, Optional, Tuple

from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
import matplotlib.figure
import matplotlib.pyplot as plt
import numpy as np
import mpl_toolkits
import tqdm

from pyelsa.axdt import generate_half_sphere
from utils import *

import pyelsa as elsa

from surfaces import visualizeOrientationDensity
from fibers3d import visualizeFibers3D


def generateTest(coeffs: List[int]) -> np.ndarray:
    """
    Generate a test spherical coefficient field for visualization
    Parameters
    ----------
    coeffs : List[int]
        The spherical harmonics to synthesize
    """
    eta = np.zeros((15, len(coeffs), 1, 1))

    # Reproduces clover-like shape of |Y_2^{\pm 2}|
    for i, c in enumerate(coeffs):
        eta[c, i, 0, 0] = 1.0

    return eta


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='AXDT Test Data Visualization')

    # parser.add_argument("--output", "-o", type=Path,
    #                     help="Path to store visualization at")

    # parser.add_argument("--interactive", "-i", action="store_true",
    #                     help="Show interactive plot")

    # parser.add_argument("--coeffs", "-c", required=True, nargs="+", type=int,
    #                     help="Pure spherical harmonics to be plotted")

    # parser.add_argument("--num", "-n", type=int, default=1,
    #                     help="Number of local maxima to plot")

    parser.add_argument("--res", "-r", type=int, default=20,
                        help="Resolution in phi and theta")

    args = parser.parse_args()

    # eta = generateTest(args.coeffs)

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(projection="3d")

    dirs, E = generate_half_sphere(args.res, args.res)

    for (i, j) in E:
        ax.plot([dirs[i, 0], dirs[j, 0]], [dirs[i, 1], dirs[j, 1]], [
            dirs[i, 2], dirs[j, 2]], color='black')

    plt.show()

    # magnitudes, indices, directions = extractFibers(
    #     eta, args.num, (args.res, args.res), 4, True)

    # visualizeOrientationDensity(ax, eta, -10, (args.res, args.res), 4)
    # visualizeFibers3D(ax, magnitudes, indices, directions, args.num, -1)

    # set_axes_equal(ax)
    # ax.set_box_aspect([1, 1, 1])

    # fig.suptitle(f"AXDT test visualization")

    # if args.output is not None:
    #     fig.savefig(args.output)

    # if args.interactive:
    #     plt.show()
    # else:
    #     plt.close(fig)
