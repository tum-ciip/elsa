#!/usr/bin/env python3
import argparse

from math import log
from pathlib import Path
from tkinter import Y

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from utils import *

import pyelsa as elsa


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Loss Calculator')

    parser.add_argument("files", type=Path, nargs="+",
                        help="Log files")

    parser.add_argument("--logX", action="store_true",
                        help="Logarithmic scale for x axis")

    parser.add_argument("--logY", action="store_true",
                        help="Logarithmic scale for y axis")

    parser.add_argument("--wallclock", "-t", action="store_true",
                        help="Use wallclock time for x axis")

    args = parser.parse_args()

    # Load the scattering functions
    logs = [pd.read_csv(f) for f in args.files]

    fig, ax = plt.subplots()

    for f, log in zip(args.files, logs):

        split = dict(tuple(log.groupby('stepsize')))

        for stepsize, l in split.items():

            x = l["time"] if args.wallclock else l["iteration"]
            y = l["loss"]

            # y /= max(y)

            ax.scatter(x, l["loss"], label=f"{
                       Path(f).stem}-$\\lambda={stepsize}$")

    if args.wallclock:
        ax.set_xlabel("Wallclock time (s)")
    else:
        ax.set_xlabel("Iteration")

    ax.set_ylabel("Loss")

    if args.logX:
        ax.set_xscale("log")
    if args.logY:
        ax.set_yscale("log")

    ax.legend()
    ax.set_title(f"Relative loss development")
    plt.show()
