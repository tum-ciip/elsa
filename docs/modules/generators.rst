***************
elsa generators
***************

.. contents:: Table of Contents

Phantoms
========

.. doxygenfunction:: elsa::phantoms::modifiedSheppLogan
   :project: elsa

.. doxygenfunction:: elsa::phantoms::forbildHead
   :project: elsa

.. doxygenfunction:: elsa::phantoms::forbildAbdomen
   :project: elsa

.. doxygenfunction:: elsa::phantoms::forbildThorax
   :project: elsa


CircleTrajectoryGenerator
=========================

.. doxygenclass:: elsa::CircleTrajectoryGenerator


SphereTrajectoryGenerator
=========================

.. doxygenclass:: elsa::SphereTrajectoryGenerator

PlanarHelixTrajectoryGenerator
========================

.. doxygenclass:: elsa::PlanarHelixTrajectoryGenerator

CurvedHelixTrajectoryGenerator
========================

.. doxygenclass:: elsa::CurvedHelixTrajectoryGenerator
