import argparse
from typing import Tuple, Union, List
import numpy as np

from .pyelsa_core import *
from .pyelsa_functionals import *
from .pyelsa_generators import *
from .pyelsa_io import *
from .pyelsa_operators import *
from .pyelsa_proximal_operators import *
from .pyelsa_line_search import *
from .pyelsa_solvers import *
from .pyelsa_projectors import *
from .pyelsa_axdt import *
from .pyelsa_axdt import extractFibers as extractFibers_cpp

DC = DataContainer


def setupProblem(recon_type: str, axdt_op: AXDTOperator, absorp_op: LinearOperator, ffa: DC, ffb: DC, a: DC, b: DC, period: int) -> Union[LeastSquares, ExpLeastSquares, RicianLoss, ConditionalRicianLikelihood]:
    """Setup an optimization problem given the specific noise assumption and return

    Parameters
    ----------
    recon_type : {"0", "1", "2a", "2b"}
        Reconstruction type describing the noise model. See Notes
    axdt_op : LinearOperator
        Operator that describes the AXDT setup
    ffa : DC
        Reference mean intensity of stepping at each position (acquired without object in beam)
    ffb : DC
        Reference mean amplitude of stepping at each position (acquired without object in beam)
    a : DC
        # Mean intensity of stepping at each position (acquired with object in beam)
    b : DC
        Mean amplitude of stepping at each position (acquired with object in beam)

    Notes
    -----
    We currently support 4 different models for noise in AXDT.
    0   The dark-field signal follows a Gaussian distribution
    1   The logarithm of the dark-field signal follows a Gaussian distribution
    2a  The mean amplitude follows a Rician distribution, but it's approximated by a Gaussian
    2b  The mean amplitude follows a Rician distribution
    """

    if recon_type in ["0", "1"]:
        d = b * ffa / (a * ffb)

    if recon_type == '0':
        # argmin |Ax - b|^2 = |B eta + ln(d)|^2
        return LeastSquares(axdt_op, -log(d))
    elif recon_type == '1':
        # argmin |exp(-B eta) - d|^2
        return ExpLeastSquares(axdt_op, d)
    elif recon_type == '2a':
        # Gaussian approximation of Rician loss
        return RicianLoss(
            ffa, ffb, a, b, absorp_op, axdt_op, period, True)
    elif recon_type == '2b':
        # Rician loss
        return RicianLoss(
            ffa, ffb, a, b, absorp_op, axdt_op, period, False)
    elif recon_type == '3a':
        return ConditionalRicianLikelihood(
            ffa, ffb, a, b, axdt_op, period, True)
    elif recon_type == '3b':
        return ConditionalRicianLikelihood(
            ffa, ffb, a, b, axdt_op, period, False)

    elif recon_type == '0\'':
        # Taylor expansion of the weighted exponential least squares term
        A = Scaling(ffb/ffa * sqrt(a)) * axdt_op
        y = ffb/ffa * sqrt(a) - b/sqrt(a)
        return LeastSquares(A, y)
    else:
        raise ValueError(f"Unknown reconstruction type: {recon_type}")


def generate_half_sphere(theta_res: int, phi_res: int) -> Tuple[List[np.ndarray], List[Tuple[int, int]]]:
    """
    Generate a unit half sphere sphere with theta_res x phi_res directions and associated edges along longitude and latitude lines (topologically equivalent to S^2 / Z_2)
    """

    assert (theta_res > 2)
    # Only even number of phi_res allows us to identify antipodes simply by ignoring them
    assert (phi_res % 2 == 0)

    print(f"Using (θ×φ)={theta_res}×{phi_res} sampling directions...")

    theta = np.linspace(0,
                        np.pi/2, theta_res, endpoint=True)

    # Remove duplicated poles as well as equator
    theta = theta[1:]

    phi = np.linspace(0, 2*np.pi, phi_res, endpoint=False)

    phi, theta = np.meshgrid(phi, theta)

    x = np.sin(theta) * np.cos(phi)
    y = np.sin(theta) * np.sin(phi)
    z = np.cos(theta)

    directions = list(
        np.stack([x.flatten(), y.flatten(), z.flatten()], axis=1))
    edges = []

    theta_res -= 1

    def indx(t, p):
        return (t % theta_res) * phi_res + (p % phi_res)

    for t in range(0, theta_res-1):
        for p in range(phi_res):
            edges.append((indx(t, p), indx(t+1, p)))
            edges.append((indx(t, p), indx(t, p+1)))

    # Add pole
    directions.append(np.array([0, 0, 1]))

    # Connect it to first circle of lattitude
    for p in range(phi_res):
        edges.append((p, len(directions)-1))

    # Add equator (few double directions don't hurt us)

    for p in range(phi_res):
        edges.append((indx(theta_res-1, p), indx(theta_res-1, p+1)))

    return directions, edges


def makeEtaContainer(eta: np.ndarray, truncL: int) -> DataContainer:

    maxCoeffs, sz, sy, sx = eta.shape

    numCoeffs = SphericalCoefficientsDescriptor.coefficientCount(
        Symmetry.Even, truncL)

    if numCoeffs > eta.shape[0]:
        print(
            f"Not enough coefficients in eta ({maxCoeffs}) to visualize, would need {numCoeffs}!")
        sys.exit(1)

    print(f"Using max l={truncL} -> {numCoeffs}/{maxCoeffs} coefficients...")

    volumeDescriptor = VolumeDescriptor([sx, sy, sz])
    sphericalCoeffDescriptor = SphericalCoefficientsDescriptor(
        volumeDescriptor, Symmetry.Even, truncL)

    return DataContainer(eta, sphericalCoeffDescriptor)


def extractFibers(eta: np.ndarray, numFibers: int, resolution: Tuple[int, int], maxDegree: int, clip: bool) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Extract fibers from the spherical coefficients by finding local maxima of the Funk-Radon transform (of the optionally clipped scattering strength eta)
    """

    # Directions to sample and edge relation
    directions, edges = generate_half_sphere(*resolution)

    etaDC = makeEtaContainer(eta, maxDegree)

    print(
        f"Extracting {numFibers} fibers for each of {eta.shape[1:]} voxels...")

    mag, indices = extractFibers_cpp(
        etaDC, directions, edges, numFibers, clip)

    mag = np.asarray(mag)
    indices = np.asarray(indices)

    print(
        f"Extracted an average of {np.count_nonzero(mag) / mag.size:.2g} fibers per voxel...")

    return mag, indices, directions
